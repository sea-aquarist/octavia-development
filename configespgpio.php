<?php
include "connection.php";
include "header.php";
include "nav.php";

//$myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
//		$thisnode = fgets($myfile);
//		$thisnode = str_replace('`', '', $thisnode);
//		fclose($myfile);
//		$thisnode = trim($thisnode);
  //      $value=$thisnode;

?>
<div class="uk-container">
    <div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">GPIO Management (ESP - Anywhere)</h3>
<form action="submit.php" method="POST">
    <input name="option" value="espupdate" hidden>
<input id="" name="frompage" value="configespgpio.php" hidden >
<?php
// ADD SENSORS TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
print '<div class="ukTableCard">';

print '<div class="">
<div class="container">
<div class="uk-button uk-button-default save-button" onclick="window.location.href =\'addespgpio.php\';">ADD ESP GPIO</div><br>
<table class="uk-table">
<thead>
<th>Ipaddr</th>
<th>ESP GPIO Number</th>
<th>Description</th>
<th>Polarity</th>
<th style="text-align:center;color: red;max-width:1px;">DEL</th>
</thead>';
$stmt3 = $db->query("SELECT * from esp WHERE node='$thisnode';");
while($row3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
        $id = $row3['id'];
        $espNumber = $row3['number'];
        $espDescription = $row3['description'];
        $espPolarity = $row3['polarity'];
        $espIpaddr = $row3['ipaddr'];
        

print '
<input name="id[]" value="'.$id.'" hidden>
<tr>
<td><input class="uk-input" name="espIpaddr[]" value="'.$espIpaddr.'"></td>
<td><input class="uk-input" name="espNumber[]" value="'.$espNumber.'" type="number"></td>
<td><input class="uk-input" name="espDescription[]" value="'.$espDescription.'"></td>
<td><input class="uk-input" name="espPolarity[]" value="'.$espPolarity.'" min="0" max ="1" type="number"></td>
<td style="width:20px !important;"><input  class="uk-checkbox delete-checkbox-color" type="checkbox" name="espRemove[]" value="'.$id.','.$thisnode.'"</td>
</tr>
';

};
print '
</table>
<button class= "uk-button uk-button-default save-button" type="submit">UPDATE</button>
</div>
</div>
</div>';
// ADD SENSORS TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
?>

</form>

</div></div>
<br>
<div class="uk-container">
	<div align="center">
		<strong>Hint:</strong> In order to control ESP8266's make sure you have programmed with the right Octavia .ino using Arduino IDE
	</div>
</div>