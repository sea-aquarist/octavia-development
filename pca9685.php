<?php
include "connection.php";
include "header.php";
include "nav.php";

$idarray = array();
$channelarray = array();
$rangearray = array();
$descriptionarray = array();
$typearray = array();


$stmt = $db->query("SELECT * from pca9685 where node='$thisnode';");
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    array_push($idarray,$row['id']);
    array_push($channelarray,$row['channel']);
    array_push($rangearray,$row['range']);
    array_push($descriptionarray,$row['description']);
    array_push($typearray,$row['type']);
    
};

?>

<style>
.uk-card-body {
	padding:0px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="css/slider.css">

<div class="uk-container">
<div align="center">


        <form action="process.php" method="POST" >
            <?php
            foreach ($idarray as $key => $value) {
                $id = $idarray[$key];

                $test = "pca9685,".$id;
                $stmt = $db->query("SELECT * from iffy3 WHERE `object`='$test' ;");
                $once = 1;                    
                    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        $enable = $row['enable'];
                        $unique = uniqid();
                        if ($enable == 1 and $once==1) {
                        print "<div id='message".$unique."' class='uk-alert-danger' uk-alert uk-toggle='target: #my-id; animation: uk-animation-slide-left, uk-animation-slide-bottom'>
                        <a class='uk-alert-close' uk-close></a>                        
                        <p>The PWM Below is disabled due to rule being \"ENABLED\"</p>
                    </div>
                    <script type='text/javascript'>
                        var delay=2000; setTimeout (function() {UIkit.toggle(message".$unique.").toggle();},delay);
                    </script>
                    "; $once=0;}
                    };

                if ($typearray[$key]==0) {$min='90';$max='650';}
                elseif ($typearray[$key]==1) {$min='0';$max='4095';}
                else {$min='0';$max='4095';;};
            print '
                <input name="id[]" value="'.$idarray[$key].'" hidden>
                
                <input name="channel[]" value="'.$channelarray[$key].'" hidden>
                <input name="description[]" value="'.$descriptionarray[$key].'" hidden>
                <div class="container">
                <div class="uk-card uk-card-body ukTableCard">
                    <div class="sliderboxA">'.$descriptionarray[$key].'</div>
                    <div class="sliderboxC">
                        <div style="display:inline-table">Step:&nbsp;</div>
                        <div class="infosliderpwm" style="display:inline-table" id="demo'.$id.'"></div>
                        <div style="display:inline-table">&nbsp;=&nbsp;</div>
                        <div  class="infosliderpercent" style="display:inline-table" id="demopercent'.$id.'"></div>
                        <div style="display:inline-table">%</div>
                    </div>                    
                    <div class="sliderboxB">
                        <input id="myRange'.$id.'" min="'.$min.'" max="'.$max.'" class="slider" name="range[]" type="range" value="'.$rangearray[$key].'" onchange="mySubmit(this.form)" ... >                
                    </div>                                      
                </div>
                </div>
                <br>
                
                ';
            };
            ?>
        <button type="submit" hidden>GO</button>
        </form>
</div>

<div id="returnmessage"></div>

<script>
function mySubmit(theForm) {
    $.ajax({ // create an AJAX call...
        data: $(theForm).serialize(), // get the form data
        type: $(theForm).attr('method'), // GET or POST
        url: $(theForm).attr('action'), // the file to call
        success: function (response) { // on success..
            $('#returnmessage').html(response); // update the DIV
        }
    });
}   

</script>


<?php
foreach ($idarray as $key => $value) {
print '
<script>
var slider'.$value.' = document.getElementById("myRange'.$value.'");
var output'.$value.' = document.getElementById("demo'.$value.'");
var outputpercent'.$value.'= document.getElementById("demopercent'.$value.'");
output'.$value.'.innerHTML = slider'.$value.'.value; // Display the default slider value
// Update the current slider value (each time you drag the slider handle)
outputpercent'.$value.'.innerHTML = Math.round(slider'.$value.'.value  * 100 / 4096);
slider'.$value.'.oninput = function() {
  output'.$value.'.innerHTML = this.value;
  outputpercent'.$value.'.innerHTML = Math.round(this.value * 100 / 4096);   
}
    </script>';

};

?>
</div>


<script type="text/javascript">
var delay=4000;
setTimeout (function() {
    UIkit.toggle(message).toggle();
},delay);
</script>
