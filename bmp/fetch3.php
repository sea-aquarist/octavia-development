<!-- <script src="../js/jquery.js"></script> -->
<!-- <link rel="stylesheet" href="../css/uikit.min.css" /> -->
<script src="//cdn.rawgit.com/Mikhus/canvas-gauges/gh-pages/download/2.1.5/all/gauge.min.js"></script>

<canvas id="canvas-id"></canvas>
<script type="text/javascript">
	var gauge = new RadialGauge({
    renderTo: 'canvas-id',
    width: 300,
    height: 300,
    units: "Km/h",
    minValue: 940,
    maxValue: 1050,
    majorTicks: [
        "940",
        "945",
        "950",
        "955",
        "960",
        "965",
        "970",
        "975",
        "980",
        "985",
        "990",
        "995",
        "1000",
        "1010",
        "1015",
        "1020",
        "1025",
        "1030",
        "1035",
        "1040",
        "1045",
        "1050",
    ],
    minorTicks: 10,
    strokeTicks: true,
    highlights: [
        {
            "from": 0,
            "to": 23,
            "color": "rgba(200, 50, 50, .75)"
        }
    ],
    colorPlate: "#fff",
    borderShadowWidth: 0,
    borders: false,
    needleType: "arrow",
    needleWidth: 2,
    needleCircleSize: 7,
    needleCircleOuter: true,
    needleCircleInner: false,
    animationDuration: 1500,
    animationRule: "linear",
    value:900
}).draw()

</script>
