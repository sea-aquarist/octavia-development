<!-- <script src="../js/jquery.js"></script> -->
<!-- <link rel="stylesheet" href="../css/uikit.min.css" /> -->
<!-- <script src="//cdn.rawgit.com/Mikhus/canvas-gauges/gh-pages/download/2.1.5/all/gauge.min.js"></script> -->

<style type="text/css">
	.fonttheme {
		color:white;
	}
	html {background-color: #0d0d0d !important;

	}
</style>

<?php
$myfile = fopen("/mnt/ramdrive/data.log", "r") or die("Unable to open file!");
$data = fgets($myfile);
fclose($myfile);
// print '<div class="fonttheme">'.$data.'</div>';
$dataset = explode(",", $data);
$value = $dataset[0];
// DHT

$humid = $dataset[4];

// DHT
$temperature = $dataset[2];
$pressure = $value / 100;
$textnum = $pressure;
$textchange = strval($textnum);
$splitresult = (explode(".", $textchange));
$trailingdecimal=$splitresult[1];
// print $trailingdecimal;
?>

<!doctype html>
<html>


   <link rel="stylesheet" type="text/css" href="dash.css">

<?php
include "connection.php";
function bracketbox($title,$data,$label) {
  print '
  <div class="bracketbox">
    <div class="bracket_title">'.$title.'</div>
    <div id="left">&nbsp;</div>
    <div class="middle"  style="padding-left:15px; padding-right:15px;">'.$data.'&#176;</div> 
    <div id="right">&nbsp;</div>
    <div class="bracket_bottom_title_therm">'.$label.'</div>
  </div>
  ';
};

function bracketbox_type_2_dht($title,$data,$pwmpercent,$rpm) {
  print '
  <div class="bracketbox" >
    <div class="bracket_title">'.$title.'</div>
    <div id="left">&nbsp;</div>
    <div class="middle" style="padding-left:25px; padding-right:25px;">'.$data.'%</div> 
    <div id="right">&nbsp;</div>
    <div class="bracket_bottom_title_dht">FAN@<font style=";color:red;">'.$pwmpercent.'% PWM</font> = '.$rpm.'</div>
  </div>
  ';
};

function brickprint($x,$y,$z,$a) {
      print '<link href="dash.css" rel="stylesheet">';             
      $originalvalue = $x;
      if ($a==0) {$x=$x*10/100;};
      if ($a==1) {$x= (1060-$x) * 100 / 130 / 10; $x = intval($x);};
      
      $colorbrickarray = array("#00cc00","#00ff00","#00ff00","#00ff00","#ffff00","#ffff00","#ff6600","#ff0000","#ff0000","#ff0000");
      $collect = array();
      print '<div class="carddash" style="display:inline-table;"><div class="headerdash">';
      $colorcount = 0;
          foreach ($colorbrickarray as $key => $value) {                
                if ($colorcount >= $x) {array_push($collect, "#333333");} else {array_push($collect, $value);};
                $colorcount = $colorcount+1;
                  };
      print "Low ";
      foreach ($collect as $key => $value) {
          print '<div id="brick" style="color:'.$value.';text-align:center;display:inline-table;letter-spacing: 2px;">█</div>';
        };
      print " High";
      print '<div align="center">'.$originalvalue.'</div>';
      
      print'</div>';
      print '<div class="containerdash">';
      print '<span class="yellowtext" style="font-size:0.9em;">'.$y.'<br>
      <font style="font-size:0.7em;"><font style="font-size:1.5em;font-weight:bold;color:white">'.$z.'</font></font></span>';
      
      print '</div>';
          print '</div>';

};

?>

<div align="center">

<?php



bracketbox("",$pressure,"BAROMETER");
bracketbox("",$temperature,"TEMPERATURE");
bracketbox("",$humid,"HUMIDITY");
print "<br>";
brickprint($humid,"Humidity","Percentage %",0);
brickprint($pressure,"Barometer","Pressure",1);
print "<br>";

?>



</div>