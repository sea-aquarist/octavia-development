
   <link rel="stylesheet" type="text/css" href="dash.css">

<?php
include "connection.php";
function bracketbox_type_1_therm($title,$data) {
  print '
  <div class="bracketbox">
    <div class="bracket_title">'.$title.'</div>
    <div id="left">&nbsp;</div>
    <div class="middle"  style="padding-left:15px; padding-right:15px;">'.$data.'&#176;</div> 
    <div id="right">&nbsp;</div>
    <div class="bracket_bottom_title_therm">THERMOSTAT</div>
  </div>
  ';
};

function bracketbox_type_2_dht($title,$data,$pwmpercent,$rpm) {
  print '
  <div class="bracketbox" >
    <div class="bracket_title">'.$title.'</div>
    <div id="left">&nbsp;</div>
    <div class="middle" style="padding-left:25px; padding-right:25px;">'.$data.'%</div> 
    <div id="right">&nbsp;</div>
    <div class="bracket_bottom_title_dht">FAN@<font style=";color:red;">'.$pwmpercent.'% PWM</font> = '.$rpm.'</div>
  </div>
  ';
};
bracketbox_type_1_therm("Title","45");
?>
