
<?php
include "connection.php";
include "header.php";
include "nav.php";

// $addpitables = array();
// array_push($addpitables, 'masterrelay');
// $stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
// 	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
// 		$tablename = $row['Tables_in_octavia'];
// 		array_push($addpitables, $tablename);
// 	;};



?>


<div class="livedata"></div>


<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  RefreshData();
  setInterval( RefreshData, 500 );
  var inRequestb = false;
  function RefreshData() {
    if ( inRequestb ) {
      return false;
    }
    inRequestb = true;
    var load = $.get('diagnosticslive.php');
    $(".livedata");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".livedata").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestb = false;
    });
  }
</script>
