
<?php
// $noderequest = $_GET['node'];
include "connection.php";
include "header.php";
include "nav.php";

$addpitables = array();
array_push($addpitables, 'masterrelay');
$stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		$tablename = $row['Tables_in_octavia'];
		array_push($addpitables, $tablename);
	;};

// $onchange = "";
// $noderequest_strip = str_replace("node_", "", $noderequest);
// $noderequest_strip = str_replace("_", " ", $noderequest_strip);


$ds18b20_id_array = array();    
$gpio_id_array = array();
$esp_id_array = array();
$dht1122_id_array = array();

        $stmt = $db->query("SELECT id FROM ds18b20 WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("ds18b20,".$row['id']."");
            array_push($ds18b20_id_array, $id);
        };

        $stmt = $db->query("SELECT id FROM dht1122 WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("dht1122,".$row['id']."");
            array_push($dht1122_id_array, $id);
        };

        $stmt = $db->query("SELECT id FROM gpio WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("gpio,".$row['id']."");
            array_push($gpio_id_array, $id);
        };

        $stmt = $db->query("SELECT id FROM esp WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("esp,".$row['id']."");
            array_push($esp_id_array, $id);
        };






?>
<style type="text/css">
	.stylemyinput_checkbox {
	min-height: 30px;
	min-width: 30px;		
	}
	th {
		text-align: center;
	}
 :focus {outline: none !important;}
</style>

<div class="uk-container">
    <div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Add a Testing Rule</h3>
<!-- Accordian open -->


<!-- Data -->

	<form class="" name = "addcustomrelay" action="submit.php" method="POST" style="margin:10px;padding-bottom: 10px;">
		<table class="uk-table " id="tbl_posts">
				<input name="option" value = "addtryrule" hidden>
				<input name="frompage" value = "rules.php" hidden>
				<input name="fromnode" value="<?php print $thisnode; ?>" hidden >

				<tr>
					<thead>
							<th>Sensor To Test</th>							
							<th>Description For Test</th>
							<th>Rule Type</th>							
					</thead>
				</tr>
			<tbody id="tbl_posts_body">
			<tr id="rec-1">
<!-- REPLICATION PORTION -->
				<?php print'

				<td><select class="uk-select" name="sensor[]">';
                
               foreach($dht1122_id_array as $key => $value) {
                    $x = explode(",",$value);
                        
                            $stmt2 = $db->query("SELECT * from dht1122 WHERE  id='$x[1]';");
                                while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $dhtname=$row2['description'];                                    
                                            $dhtDescription=$row2['description'];
                                    };                    
                    print '<option value="'.$value.'">DHT: ('.$dhtname.')</option>';
                };
                
               foreach($ds18b20_id_array as $key => $value) {
                    $x = explode(",",$value);
                        
                            $stmt2 = $db->query("SELECT * from ds18b20 WHERE  id='$x[1]';");
                                while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $ds18b20name=$row2['description'];
                                            $ds18b20Description=$row2['description'];                                    
                                    };                  
                    print '<option value="'.$value.'">DS18B20: ('.$ds18b20name.')</option>';
                };

                foreach($gpio_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    
                        $stmt2 = $db->query("SELECT * from gpio WHERE  id='$x[1]';");
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        $gpioNumber=$row2['number'];
                                        $gpioDescription=$row2['description'];                                    
                                };               
                    print '<option value="'.$value.'">GPIO: '.$gpioNumber.' ('.$gpioDescription.')</option>';
                        };

                foreach($esp_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    
                        $stmt2 = $db->query("SELECT * from esp WHERE  id='$x[1]';");
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        $espNumber=$row2['number'];
                                        $espDescription=$row2['description'];                                    
                                };               
                    print '<option value="'.$value.'">ESP: '.$espNumber.' ('.$espDescription.')</option>';
                        };
print '<option value="time,0">Time</optoin>';
            print '</select></td>';
				
				?>

				<td><input class="uk-input" name="description[]"></td>
				
				<td>
                    
                    <select class="uk-select" name="ruletype[]">
                        <option value="0" '.$r0.'>Between Values</option>
                        <option value="1" '.$r1.'>Between Time</option>
                        <option value="2" '.$r2.'>Is Equal To</option>                        
                        <option value="3" '.$r3.'>GPIO is</option>                        
                    </select>
               
               </td>
					
<!-- REPLICATION PORTION -->
					<td  style="text-align: center;"><a class="add-record" data-added="0"><i class="uk-icon-link" uk-icon="plus" style="color: lightgreen;"></a></td>

          		</tr>
			</tbody>
			
			</table>
			
			<button class="<?php print $theme;?> uk-button uk-button-default save-button" type="submit">Add</button>
		</form>


</div>
       
       <br>

<div style="display:none;">
    <table id="sample_table">
      <tr id="">
<!-- REPLICATION PORTION -->
			<?php print'

				<td><select class="uk-select" name="sensor[]">';
                
               foreach($dht1122_id_array as $key => $value) {
                    $x = explode(",",$value);
                        
                            $stmt2 = $db->query("SELECT * from dht1122 WHERE  id='$x[1]';");
                                while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $dhtname=$row2['description'];                                    
                                            $dhtDescription=$row2['description'];
                                    };                    
                    print '<option value="'.$value.'">DHT: ('.$dhtname.')</option>';
                };
                
               foreach($ds18b20_id_array as $key => $value) {
                    $x = explode(",",$value);
                        
                            $stmt2 = $db->query("SELECT * from ds18b20 WHERE  id='$x[1]';");
                                while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                            $ds18b20name=$row2['description'];
                                            $ds18b20Description=$row2['description'];                                    
                                    };                  
                    print '<option value="'.$value.'">DS18B20: ('.$ds18b20name.')</option>';
                };

                foreach($gpio_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    
                        $stmt2 = $db->query("SELECT * from gpio WHERE  id='$x[1]';");
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        $gpioNumber=$row2['number'];
                                        $gpioDescription=$row2['description'];                                    
                                };               
                    print '<option value="'.$value.'">GPIO: '.$gpioNumber.' ('.$gpioDescription.')</option>';
                        };

                foreach($esp_id_array as $key => $value) {
                    $x = explode(",",$value);                    
                    
                        $stmt2 = $db->query("SELECT * from esp WHERE  id='$x[1]';");
                            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                                        $espNumber=$row2['number'];
                                        $espDescription=$row2['description'];                                    
                                };               
                    print '<option value="'.$value.'">ESP: '.$espNumber.' ('.$espDescription.')</option>';
                        };

            print '</select></td>';
				
				?>

				<td><input class="uk-input" name="description[]"></td>
				
				<td>
                    
                    <select class="uk-select" name="ruletype[]">
                        <option value="0" '.$r0.'>Between Values</option>
                        <option value="1" '.$r1.'>Between Time</option>
                        <option value="2" '.$r2.'>Is Equal To</option>                        
                        <option value="3" '.$r3.'>GPIO is</option>                        
                    </select>
               
               </td>

				
<!-- REPLICATION PORTION -->
				<td  style="text-align: center;"><a class="btn btn-xs delete-record" data-id="0"><i class="uk-icon-link" uk-icon="trash"  style="color: red;"></a></td>

     </tr>     
   </table>
 



<script type="text/javascript">
	jQuery(document).delegate('a.add-record', 'click', function(e) {
		e.preventDefault();    
		var content = jQuery('#sample_table tr'),
		size = jQuery('#tbl_posts >tbody >tr').length + 1,
		element = null,    
		element = content.clone();
		element.attr('id', 'rec-'+size);
		element.find('.delete-record').attr('data-id', size);
		element.appendTo('#tbl_posts_body');
		element.find('.sn').html(size);
	});

	jQuery(document).delegate('a.delete-record', 'click', function(e) {
		e.preventDefault();    
		// var didConfirm = confirm("Are you sure You want to delete");
		// if (didConfirm == true) {
			if (1 == 1) {
		var id = jQuery(this).attr('data-id');
		var targetDiv = jQuery(this).attr('targetDiv');
		jQuery('#rec-' + id).remove();
		
		//regnerate index number on table
		$('#tbl_posts_body tr').each(function(index) {
		//alert(index);
		$(this).find('span.sn').html(index+1);
		});
		return true;
	} else {
		return false;
	}
	});
</script>
</div>
</div>