
<?php
include "connection.php";


?>

<div class="uk-container">
<h2>Intended for support services only</h2>
<?php
print '<h3>Files in Octavia Ramdisk</h3>';
$mnt =  shell_exec('ls -lh /mnt/octavia');
print '<pre>'.$mnt.'</pre>';


print '<h3>Important Log Files</h3>';
$mnt =  shell_exec('ls -lh /var/log/ | grep -E \'syslog|daemon\'');
print '<pre>'.$mnt.'</pre>';

print '<h3>True Systemctl Status</h3>';
$mnt =  shell_exec('systemctl list-units --type=service| grep -E \'therm.service|ldd.service|schedgpio.service|schedesp.service|telegram.service|rules.service|rulesact.service|esp8266.service|dhtfetch.service|maintenance.service\'');
print '<pre>'.$mnt.'</pre>';


print '<h3>Local Pi Information</h3>';

// IP Address
print "Local Ip Address : <strong>".$_SERVER['SERVER_ADDR']."</strong>";
// IP Address


print '<br />';
// Disk Space
    $bytes = disk_free_space(".");
    $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
    $base = 1024;
    $class = min((int)log($bytes , $base) , count($si_prefix) - 1);
    // echo $bytes . '<br />';
    print "Local Disk Free Space: <strong>".sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . '</strong><br />';
 // Disk Space


// Disk Space
    $bytes = disk_free_space("/mnt/octavia");
    $si_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
    $base = 1024;
    $class = min((int)log($bytes , $base) , count($si_prefix) - 1);
    // echo $bytes . '<br />';
    print "Octavia RamDisk Free Space: <strong>".sprintf('%1.2f' , $bytes / pow($base,$class)) . ' ' . $si_prefix[$class] . '</strong><br />';
 // Disk Space

// Hostname
print "Hostname: <strong>".gethostname()."</strong>";
// Hostname

print '<pre>';
 $f = fopen("/sys/class/thermal/thermal_zone0/temp","r");
 $temp = fgets($f);
 $temp = intval($temp);
 $value = ($temp / 1000);
 print 'CPU Temperature: '.round($value);
 fclose($f);
 print '</pre>';


?>
<br><br><Br>
</div>