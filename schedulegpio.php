<?php
include "connection.php";
include "header.php";
include "nav.php";

// $myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
// 		$thisnode = fgets($myfile);
// 		$thisnode = str_replace('`', '', $thisnode);
// 		fclose($myfile);
// 		$thisnode = trim($thisnode);
//         $value=$thisnode;
         $value=$thisnode;

         $gpio_id_array = array();
$stmt = $db->query("SELECT id FROM gpio WHERE node='$thisnode';");
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $id = ("gpio,".$row['id']."");
    array_push($gpio_id_array, $id);
};
?>

<div id="modal-container" class="uk-modal-container" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <h2 class="uk-modal-title">Scheduled GPIO/ESP Functionality</h2>
        <h4>Time</h4>
        <p>It is really important that you have your local time set correctly on your Raspberry Pi, the system is heavily dependant on that.  To ensure that your timezon and locale are correct, run sudo raspi-config and validate.</p>
        <h4>Day</h4>        
        <p>Select the days you your GPIO or ESP should be active/inactive according to settings.  If you are on a PC or Mac, it's useful to know that you can do multiple selects using your CTRL or Command button while you press your selections with your mouse. By default your GPIO will trigger daily.</p>
        <h4>Cycle</h4>
        <p>The cycle function works provided certian conditions are met.  First you need to set the Duration that it will be active for, and then how long the Rest period will be.  If we set the Duration:10 and Rest:5 it means, that it will be active for 10 seconds and then off for 5 seconds then repeat.  It will repeat based on the time peroid set.  And will only work with the In Zone condition being on, and the Out Zone condition being off.</p>
        <p>Any other configurations are illogical, in terms of how it behaves.  Setting your Duration to 0 and Rest to 0, will disable this ability.  The icon will also change color to green if enabled.</p>
        <p>Another option you can enabled, is the run once feature, this is useful if you only want it to run once on this day for the duration specifiec.  This is really helpful if you only want a specific "on" period once, during the InZone On Time.</p>
        <h4>GPIO/ESP</h4>
        <p>Naturally you have to add GPIO's and ESP/Gpio's to be able to select them.  If you have not done so, you will see none in the selector.</p>
        <h4>In Zone</h4>
        <p>In Zone denotes, the time between the Begin Zone and the End Zone, this features provides you the ability to specify "when in zone, do x".</p>
        <h4>Out Zone</h4>
        <p>Out Zone denotes, the time outside of the range between Begin Zone and the End Zone, this features provides you the ability to specify "when out zone, do x"</p>
        <h4>Alerts</h4>
        <p>Telegram Alerts On/Off</p>
        <h4>Del</h4>
        <p>Delete this entry.</p>
        <h4>Del</h4>
        <h4>View Scheduler</h4>
        <p>The scheduler allows you to see a visual representation of what will transpire.  The scheduler has no other function that to help you see you zone settings on a daily basis/weekly and in comparisson to others.</p>

    </div>

</div>


    
<div class="uk-container">
<form action="submit.php" method="POST">
<input name="option" value="rigidrelayupdate" hidden>

<div class="uk-card uk-card-default uk-card-body">    
    <div>
        <div style="display: inline-table;"><h3 class="uk-card-title">Scheduled GPIO Management</h3></div>
        <div style="display: inline-table;float: right;"><a class="" href="#modal-container" uk-toggle><span uk-icon="icon: question;"></span></a></div>
    </div>
    <hr style="margin-top:10px;">

<input id="" name="frompage" value="schedulegpio.php" hidden >

<?php

// ADD RELAY TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
print '<div class="ukTableCard">';

print '<div class="" style="padding-bottom:10px;">';
print '<input type="" name="nodename" value="'.$value.'" hidden>
        <div class="container">
        <div class="uk-button uk-button-default save-button" onclick="window.location.href =\'addchannel.php?node='.$thisnode.'\';">ADD</div><br>
        <table id="" class="uk-table">
        <tr>
        <thead>
            <th >Description</th>
            
            <th  class="sorter-false">Day</th>
            <th  class="sorter-false">Cycle</th>            
            <th  class="sorter-false">Gpio</th>           
            <th  hidden class="sorter-false"><strong>in</strong>&nbsp;zone</th>
            <th  hidden class="sorter-false"><strong>out</strong>&nbsp;zone</th>
            <th  class="sorter-false">Mode</th>
            <th >Begin Zone</th>
            <th >End Zone</th>
            <th  class="sorter-false"style="text-align:center;color: red;max-width:1px;">ALERT</th>    
            <th  class="sorter-false"style="text-align:center;color: red;max-width:1px;">DEL</th>					
        </thead>
        </tr>
';
print '<tbody>';


$relay_array = array();
$esp_id_array = array();


$stmt = $db->query("SELECT id FROM esp WHERE node='$thisnode';");
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $id = ("esp,".$row['id']."");
            array_push($esp_id_array, $id);
        };


 

$stmt = $db->query("SELECT * from octavia.`$value` ORDER BY objectname,time(beginning) ;");while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
$id = $row['id']; array_push($relay_array, $id);
$description = $row['description'];
$object = $row['objectname'];
$day = $row['day'];
$intime = $row['intime'];
$outtime = $row['outtime'];
$hold = $row['hold'];
$alert = $row['alert'];
if ($alert==1) {$alertresult="checked";} else {$alertresult="";};
// $polarity = $row['polarity'];
$begining = $row['beginning'];
$ending = $row['ending'];
// $esp = $row['esp'];
$begining = strtotime($begining);
$begining = date("H:i:s",$begining);
$ending = strtotime($ending);
$ending = date("H:i:s",$ending);
$now = date("H:i:s");
$cycleduration = $row['cycleduration'];
if ($cycleduration>0) {$cycleduration_style="background-color:#ddffd9;";} else {$cycleduration_style="background-color:none;";};
$cycleinterval = $row['cycleinterval'];
$once=$row['once'];
$onceoption=$row['onceoption'];

// $end = date("H:i:s",$end);

if ($onceoption == "1") {$runonce_select_y="selected";$runonce_select_n="";};
if ($onceoption == "0") {$runonce_select_n="selected";$runonce_select_y="";};
if ($once == "1") {$runoncestatus="<i style='color:lightgreen'>Yes</i>";} else {$runoncestatus="<i style='color:lightyellow'>No</i>";};



if ($intime == "2") {$intime_select_skip="selected";$intime_select_y="";$intime_select_n="";};
if ($intime == "1") {$intime_select_y="selected";$intime_select_n="";$intime_select_skip="";};
if ($intime == "0") {$intime_select_n="selected";$intime_select_y="";$intime_select_skip="";};

if ($outtime == "2") {$outtime_select_skip="selected";$outtime_select_y="";$outtime_select_n="";};
if ($outtime == "1") {$outtime_select_y="selected";$outtime_select_n="";$outtime_select_skip="";};
if ($outtime == "0") {$outtime_select_y="";$outtime_select_n="selected";$outtime_select_skip="";};

if ($hold == "2") {$hold_select_none="selected";$hold_select_y="";$hold_select_n="";};
if ($hold == "1") {$hold_select_y="selected";$hold_select_n="";$hold_select_none="";};
if ($hold == "0") {$hold_select_n="";$hold_select_n="selected";$hold_select_none="";$hold_select_y="";};

$dayarray = str_split($day, "1");

$zero='';
$one='';
$two='';
$three='';
$four='';
$five='';
$six='';
$eight='';

$mhighlight=	"color:red;";
$thighlight=	"color:red;";
$whighlight=	"color:red;";
$thhighlight=	"color:red;";
$fhighlight=	"color:red;";
$sathighlight=	"color:red;";
$sunhighlight=	"color:red;";
$selectColor = "lightgreen";

foreach ($dayarray as $key => $value) {
    if ($value=="0") {$zero="selected";	$mhighlight=	"font-size:1em;color: ".$selectColor.";font-weight: bold;;";};
    if ($value=="1") {$one="selected";	$thighlight=	"font-size:1em;color: ".$selectColor.";font-weight: bold;;";};	
    if ($value=="2") {$two="selected";	$whighlight=	"font-size:1em;color: ".$selectColor.";font-weight: bold;;";};	
    if ($value=="3") {$three="selected";$thhighlight=	"font-size:1em;color: ".$selectColor.";font-weight: bold;;";};	
    if ($value=="4") {$four="selected";	$fhighlight=	"font-size:1em;color: ".$selectColor.";font-weight: bold;;";};	
    if ($value=="5") {$five="selected";	$sathighlight=	"font-size:1em;color: ".$selectColor.";font-weight: bold;;";};	
    if ($value=="6") {$six="selected";	$sunhighlight=	"font-size:1em;color: ".$selectColor.";font-weight: bold;";};		
    if ($value=="8") {$eight="selected";$all=			"font-size:1em;color: ".$selectColor.";font-weight: bold;";$mhighlight=$all;$thighlight=$all;$whighlight=$all;$thhighlight=$all;$fhighlight=$all;$sathighlight=$all;$sunhighlight=$all;};
    # code...
    };
print '
<tr>
<td><input name="description[]'.$id.'" class="uk-input" value="'.$description.'"><info hidden>'.$description.'</info></td>

<td >

<button class="scheduleButtonDay uk-button " type="button" uk-toggle="target: #offcanvas-'.$id.'">

<div class="scheduleButtonFont">
<font style="'.$mhighlight.'">Mon</font>
<font style="'.$thighlight.'">TUE</font>
<font style="'.$whighlight.'">WED</font>
<font style="'.$thhighlight.'">THU</font><br>
<font style="'.$fhighlight.'">FRI</font>
<font style="'.$sathighlight.'">SAT</font>
<font style="'.$sunhighlight.'">SUN</font>
</div>
</font></button>

<div id="offcanvas-'.$id.'" uk-offcanvas="flip: true; overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <h3>Frequency</h3>
        <input name="day[]" value="|" hidden >
        <select multiple name="day[]'.$id.'" class="uk-select" style="min-height:160px;">		
            <option value="0" '.$zero.'>Mon</option>
            <option value="1" '.$one.'>Tue</option>
            <option value="2" '.$two.'>Wed</option>
            <option value="3" '.$three.'>Thu</option>
            <option value="4" '.$four.'>Fri</option>
            <option value="5" '.$five.'>Sat</option>
            <option value="6" '.$six.'>Sun</option>			
            <option value="8" '.$eight.'>Daily</option>	
        </select>
        <p>To save these selections click SUBMIT below, or you can continue to make more changes in the in the main window, and commit changes when UPDATE is clicked.</p><p>For multiple selections using a PC hold CTRL and select items.</p>
        <button class="uk-button uk-button-default save-button" type="submit">Submit</button>
    </div>
</div>

</td>
<td>
<button class="scheduleButton uk-button" style="'.$cycleduration_style.'" type="button" uk-toggle="target: #offcanvas-'.$id.$id.'">
<img src="assets/bike.png" style="max-width:40px;">
</button>

<div id="offcanvas-'.$id.$id.'" uk-offcanvas="flip: true; overlay: true">
    <div class="uk-offcanvas-bar">
        <button class="uk-offcanvas-close" type="button" uk-close></button>
        <h3>Additional Attributes</h3>
        <h4>Cycle Time</h4>
        <p>The following is recorded in seconds, for example 300 seconds = 5 min</p>
        <p>Setting the values to 0 will cancel this operation.  However it will not disable the schedule items normal operational properties.</p>
        <p>Duration(s)<input class="uk-input" name="cycleduration[]" value="'.$cycleduration.'"></p>
        <p>Rest(s)<input class="uk-input" name="cycleinterval[]" value="'.$cycleinterval.'"></p>
        Run Once
        <select   name="runonce[]'.$id.'" class="uk-select">
        <option value="1" '.$runonce_select_y.'>ON</option>
        <option value="0" '.$runonce_select_n.'>OFF</option>
        </select>
        <p>Has it run once today: '.$runoncestatus.'</p>
        The status above is not dyanmic a page <a href="javascript:location.reload(true)">Refresh</a> might be required.
        

        <p><button type="submit" class= "uk-button uk-button-default save-button">UPDATE</button></p>
    </div>
</div>
</td>
';

print '<td><select class="uk-select" name="gpio_id[]">';
foreach($gpio_id_array as $key => $value) {    
    $x = explode(",",$value);                    
    if ($object == $value){$selected = "selected";}else{$selected="";};
        $stmt2 = $db->query("SELECT * from gpio WHERE  id='$x[1]';");
            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                        $gpioNumber=$row2['number'];
                        $gpioDescription=$row2['description'];                                    
                };               
    print '<option value="'.$value.'" '.$selected.'>GPIO: '.$gpioNumber.' ('.$gpioDescription.')</option>';
        };
    
foreach($esp_id_array as $key => $value) {
    $x = explode(",",$value);                    
    if ($object == $value){$selected = "selected";}else{$selected="";};
        $stmt2 = $db->query("SELECT * from esp WHERE  id='$x[1]';");
            while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
                    $espNumber=$row2['number'];
                    $espDescription=$row2['description'];                                    
                };               
         print '<option value="'.$value.'" '.$selected.'>ESP: '.$espNumber.' ('.$espDescription.')</option>';
            };

print '</select></td>';

print '


<td hidden><select   name="intime[]'.$id.'" class="uk-select">
        <option value="2" '.$intime_select_skip.'>SKIP</option>
        <option value="1" '.$intime_select_y.'>ON</option>
        <option value="0" '.$intime_select_n.'>OFF</option>
    </select>
</td>
<td hidden><select   name="outtime[]'.$id.'" class="uk-select">
        <option value="2" '.$outtime_select_skip.'>SKIP</option>
        <option value="1" '.$outtime_select_y.'>ON</option>
        <option value="0" '.$outtime_select_n.'>OFF</option>
    </select>
</td>
<td><select   name="hold[]'.$id.'" class="uk-select">
        <option value="2" '.$hold_select_none.'>AUTO</option>
        <option value="1" '.$hold_select_y.'>ON</option>
        <option value="0" '.$hold_select_n.'>OFF</option>
    </select>
</td>
<td><input  step="1" type="time" name="beginning[]'.$id.'" class="uk-input" value="'.$begining.'"><info hidden>'.$begining.'</info></td>
<td ><input step="1"  type="time" name="ending[]'.$id.'" class="uk-input" value="'.$ending.'"><info hidden>'.$ending.'</info></td>
<td style="width:20px !important;">
    <input  class="uk-checkbox" type="checkbox" name="alert[]" value="'.$id.'" '.$alertresult.'>    
</td>
<td style="width:20px !important;"><input  class="uk-checkbox delete-checkbox-color" type="checkbox" name="remove[]" value="'.$id.','.$thisnode.'"></td>

<input  name="id[]" value="'.$id.'" hidden>
</tr>		';
    };
    
    print '</tbody>';
print '</table>';
print '
<button type="submit" class= "uk-button uk-button-default save-button">UPDATE</button>
<a href="schedule.php" onclick="schedule.php" style="float:right;">View Schedule Calendar</a>
';

print '</div>';
print '</div>';
print '</div>';

// ADD RELAY TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

?>
</form>

</div>
</div></div>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
