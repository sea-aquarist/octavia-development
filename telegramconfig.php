<?php

include "connection.php";
include "header.php";
include "nav.php";

?>

<form action="submit.php" method="POST">
    <input name="option" value="dht1122update" hidden>
<input id="" name="frompage" value="configdht1122.php" hidden >
<div class="uk-container">
	<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Notifications - Telegram API - Channel</h3>
<?php
// ADD DHT TABLE -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$chat_id=NULL;
$token=NULL;

$stmt = $db->query("SELECT * from config WHERE node='$thisnode' AND description='chat_id';");
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $chat_id=$row['set1'];
    };

$stmt = $db->query("SELECT * from config WHERE node='$thisnode' AND description='telegramtoken';");
while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $token=$row['set1'];        
    };

if ($chat_id==NULL|$token==NULL) {print"No Config";};
?>

<form class="uk-form-horizontal uk-margin-large">
	<input name="option" value="telegramapi" hidden>
	<input name="fromnode" value="<?php print $thisnode; ?>" hidden>

<table>
<tr>
	<td>Chat ID</td>
	<td><input id="passwordtype1" class="uk-input uk-form-width-large" type="password" name="chat_id" value="<?php print $chat_id;?>" required></td>
</tr>
<tr>
	<td>Token</td>
	<td>
	<input id = "passwordtype2" class="uk-input uk-form-width-large" type="password" name="token" value="<?php print $token;?>" required>
	</td>
	
</tr>
</table>
<br>
<p><input type="checkbox" onclick="Toggle()"> 
    <b>Show Chat ID & Token ID</b></p>
<button class="uk-button uk-button-default save-button">SAVE</button>
</form>
<hr>

	<p>Once the above settings have been saved, test Telegram below, to validate configuration. If you do not see the message in Telegram, something is wrong. Either the telegram service in Octavia is not running, or the token and chat id are incorrect.</p><p>Validate and try again.</p>
<div style="">
<form action="submit.php" method="POST">
	<input type="" name="option" value="telegramtest" hidden>
	<div style="display: inline-table;">Message:</div>
	<div style="display: inline-table;"><input class="uk-input" name="message" required=""></div>
	<input type="" name="chatid" value="<?php print $chat_id ?>" hidden >
	<input type="" name="apitoken" value="<?php print $token ?>" hidden >
	<div style="display: inline-table;"><button class="uk-button uk-button-default save-button">TEST</button></div>
</form>
</div>
 
  <script> 
    // Change the type of input to password or text 
        function Toggle() { 
            var temp1 = document.getElementById("passwordtype1"); 
            var temp2 = document.getElementById("passwordtype2");
            if (temp1.type === "password") { 
                temp1.type = "text"; 
                temp2.type = "text"; 
            } 
            else { 
                temp1.type = "password"; 
                temp2.type = "password"; 
            } 
        } 
</script> 