/*
    This sketch demonstrates how to set up a simple HTTP-like server.
    The server will set a GPIO pin depending on the request
      http://server_ip/gpio/0 will set the GPIO2 low,
      http://server_ip/gpio/1 will set the GPIO2 high
    server_ip is the IP address of the ESP8266 module, will be
    printed to Serial when the module is connected.
*/

#include <Adafruit_GFX.h>

#include <Fonts/FreeMono9pt7b.h>

#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#ifndef STASSID
#define STASSID "2Gme"
#define STAPSK  "f55fd19e3d"

#include <OneWire.h>
OneWire  ds(5);  // on pin 10 (a 4.7K resistor is necessary)
#endif

//int gpiod1 = 5;
int gpiod2 = 4;
int gpiod3 = 0;
int gpiod4 = 2;
int gpiod5 = 14;
int gpiod6 = 12;
int gpiod7 = 13;
int gpiod8 = 15;

float temp;

const char* ssid = STASSID;
const char* password = STAPSK;

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

void setup() {  
  Serial.begin(9600); 







  
//  pinMode(gpiod1, OUTPUT);
  pinMode(gpiod2, OUTPUT);
  pinMode(gpiod3, OUTPUT);
  pinMode(gpiod4, OUTPUT);
  pinMode(gpiod5, OUTPUT);
  pinMode(gpiod6, OUTPUT);
  pinMode(gpiod7, OUTPUT);
  pinMode(gpiod8, OUTPUT);

//  digitalWrite(gpiod1, 0);
  digitalWrite(gpiod2, 0);
  digitalWrite(gpiod3, 0);
  digitalWrite(gpiod4, 0);
  digitalWrite(gpiod5, 0);
  digitalWrite(gpiod6, 0);
  digitalWrite(gpiod7, 0);
  digitalWrite(gpiod8, 0);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print(F("Connecting to "));
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
//  WiFi.hostname("esp");
  MDNS.begin("esptest");
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {    
    delay(100);
    Serial.print(F(".\n"));
           
  }
  Serial.println();
  Serial.println(F("WiFi connected"));

  // Start the server
  server.begin();
  Serial.println(F("Server started"));

  // Print the IP address
  Serial.println(WiFi.localIP());
  
}
  int vala; String valam;
  int valb; String valbm;
  int valc; String valcm;
  int vald; String valdm;
  int vale; String valem;
  int valf; String valfm;
  int valg; String valgm;
  int valh; String valhm;


void loop() {

  /// ds18b20
  byte i;
  byte present = 0;  
  byte data[12];
  byte addr[8];
  float celsius;
  if ( !ds.search(addr)) {
    ds.reset_search();
    delay(250);
    return;
  }
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);
  delay(1000);
  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);
for ( i = 0; i < 9; i++) {
  data[i] = ds.read();
  }
int16_t raw = (data[1] << 8) | data[0];
byte cfg = (data[4] & 0x60);
if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
temp = (float)raw / 16.0;
Serial.print(temp);
Serial.print("\n");

/// ds18b20
  
  // Check if any reads failed and exit early (to try again).
  
  
  
  

  // Check if a client has connected
  
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
//  Serial.println(F("new client"));
  client.setTimeout(500); // default is 1000
  
  // Read the first line of the request
  String req = client.readStringUntil('\r');
//  Serial.println(F("request: "));
  Serial.println(req);

  int clearscreen;
 
  String second;String third;
  
//if (req.indexOf(F("/gpio/d1off")) != -1) {  vala = 0; digitalWrite(gpiod1, vala); }
if (req.indexOf(F("/gpio/d2off")) != -1) {  valb = 0; digitalWrite(gpiod2, valb); }
if (req.indexOf(F("/gpio/d3off")) != -1) {  valc = 0; digitalWrite(gpiod3, valc); }
if (req.indexOf(F("/gpio/d4off")) != -1) {  vald = 0; digitalWrite(gpiod4, vald); }
if (req.indexOf(F("/gpio/d5off")) != -1) {  vale = 0; digitalWrite(gpiod5, vale); }
if (req.indexOf(F("/gpio/d6off")) != -1) {  valf = 0; digitalWrite(gpiod6, valf); }
if (req.indexOf(F("/gpio/d7off")) != -1) {  valg = 0; digitalWrite(gpiod7, valg); }
if (req.indexOf(F("/gpio/d8off")) != -1) {  valh = 0; digitalWrite(gpiod8, valh); }

//if (req.indexOf(F("/gpio/d1on")) != -1) {  vala = 1;  digitalWrite(gpiod1, vala);}
if (req.indexOf(F("/gpio/d2on")) != -1) {  valb = 1;  digitalWrite(gpiod2, valb);}
if (req.indexOf(F("/gpio/d3on")) != -1) {  valc = 1;  digitalWrite(gpiod3, valc);}
if (req.indexOf(F("/gpio/d4on")) != -1) {  vald = 1;  digitalWrite(gpiod4, vald);}
if (req.indexOf(F("/gpio/d5on")) != -1) {  vale = 1;  digitalWrite(gpiod5, vale);}
if (req.indexOf(F("/gpio/d6on")) != -1) {  valf = 1;  digitalWrite(gpiod6, valf);}
if (req.indexOf(F("/gpio/d7on")) != -1) {  valg = 1;  digitalWrite(gpiod7, valg);}
if (req.indexOf(F("/gpio/d8on")) != -1) {  valh = 1;  digitalWrite(gpiod8, valh);}
 



 
  if (req.indexOf(F("/gpio/alloff")) != -1) {   vala = 0, valb = 0,valc = 0,vald = 0,vale = 0,valf = 0,valg = 0,valh = 0;  } 
  if (req.indexOf(F("/gpio/allon")) != -1) { vala = 1,valb = 1,valc = 1,vald = 1,vale = 1,valf = 1,valg = 1,valh = 1;  }
  
  
  // Set LED according to the request
  
//  digitalWrite(gpiod1, vala);
  digitalWrite(gpiod2, valb);
  digitalWrite(gpiod3, valc);
  digitalWrite(gpiod4, vald);
  digitalWrite(gpiod5, vale);
  digitalWrite(gpiod6, valf);
  digitalWrite(gpiod7, valg);
  digitalWrite(gpiod8, valh);

//  vala = digitalRead(gpiod1);
  valb = digitalRead(gpiod2);
  valc = digitalRead(gpiod3);
  vald = digitalRead(gpiod4);
  vale = digitalRead(gpiod5);
  valf = digitalRead(gpiod6);
  valg = digitalRead(gpiod7);
  valh = digitalRead(gpiod8);
  delay(1000);


  Serial.println("Hello\n");
  Serial.println(temp);
  

  


  // read/ignore the rest of the request
  // do not client.flush(): it is for output only, see below

//  while (client.available()) {
    // byte by byte is not very efficient
//    client.read();
//  }


  client.print("[\n"); 
if (vala == 1 ) { valam = "{\n\"gpio\": \"1\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n},\n"; } else { valam = "{\n\"gpio\": \"1\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n},\n";}
  client.print(valam);
  client.print(WiFi.localIP());
  client.print(second); 

if (valb == 1 ) { valbm = "{\n\"gpio\": \"2\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n},\n"; } else { valbm = "{\n\"gpio\": \"2\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n},\n";}
  client.print(valbm);
  client.print(WiFi.localIP());
  client.print(second);  

 if (valc == 1 ) { valcm = "{\n\"gpio\": \"3\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n},\n"; } else { valcm = "{\n\"gpio\": \"3\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n},\n";}
  client.print(valcm);
  client.print(WiFi.localIP());
  client.print(second); 

 if (vald == 1 ) { valdm = "{\n\"gpio\": \"4\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n},\n"; } else { valdm = "{\n\"gpio\": \"4\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n},\n";}
  client.print(valdm);
  client.print(WiFi.localIP());
  client.print(second); 

 if (vale == 1 ) { valem = "{\n\"gpio\": \"5\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n},\n"; } else { valem = "{\n\"gpio\": \"5\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n},\n";}
  client.print(valem);
  client.print(WiFi.localIP());
  client.print(second); 

 if (valf == 1 ) { valfm = "{\n\"gpio\": \"6\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n},\n"; } else { valfm = "{\n\"gpio\": \"6\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n},\n";}
  client.print(valfm);
  client.print(WiFi.localIP());
  client.print(second); 

 if (valg == 1 ) { valgm = "{\n\"gpio\": \"7\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n},\n"; } else { valgm = "{\n\"gpio\": \"7\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n},\n";}
  client.print(valgm);
  client.print(WiFi.localIP());
  client.print(second); 

 if (valh == 1 ) { valhm = "{\n\"gpio\": \"8\",\n\"status\": \"1\",\n\"ipaddr\": \""; second ="\"\n}"; } else { valhm = "{\n\"gpio\": \"8\",\n\"status\": \"0\",\n\"ipaddr\": \""; second ="\"\n}";}
  client.print(valhm);
  client.print(WiFi.localIP());
  client.print(second);     
  client.print(",\n{");   

client.print("\n\"sensor\":");
  client.print ("\"");
    client.print("ds18b20");   
   client.print("\",");   
  
  client.print("\n\"temperature\":");
  client.print ("\"");
    client.print(temp);   
   client.print("\"\n");      
  client.print("}");  
  client.print("\n]");
}

 
