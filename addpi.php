
    <style type="text/css">
    	table {
		max-width: 900px !important;

	}
    </style>


<?php
include "connection.php";
include "header.php";
include "nav.php";
?>


<div class="uk-container">
<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">Add A Raspberry Pi Node</h3>

	

<div class="<?php print $theme;?> " style="">
	<form action="submit.php" method="POST">
		<input name="option" value="add" hidden>
		<p>Node: <input class="uk-input uk-form-width-medium" type="" name="serialnumber" placeholder="Node Name" required></p>		
		
	<div style="display: inline-block;">
		<button class="uk-button uk-button-default save-button" name="" type="submit">ADD</button>
	</form>
	
	  
	</div>
</div>
<br>
<?php

$addpitables = array();

$stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 

		$tablename = $row['Tables_in_octavia'];
		array_push($addpitables, $tablename);


	;};


?>
<form action="submit.php" method="POST">
<input name="option" value="delnode" hidden>
<div class="<?php print $theme;?> " style="">
<h3 class="uk-card-title">Edit Existing Nodes</h3>
<table class="uk-table">
	<tr><th>Node Name</th><th><i class="uk-icon-link" uk-icon="trash"></th></tr>
<?php
foreach ($addpitables as $key => $value) {
	$value= str_replace("node_", "", $value);
	str_replace(" ", "_", $value);
	print '<input name="originaltables[]" value='.$value.' hidden>
	<tr><td><input type="text" class="uk-input" name="tables[]" value="'.$value.'"></td><td><input class="uk-checkbox delete-checkbox-color" type="checkbox" name="remove[]" value="'.$value.'"></td></tr>
	';


};
?>
</table>
<p><button class="<?php print $theme;?> uk-button uk-button-default save-button" name="" type="submit">SAVE</button></p>
</form>
</div>
</div>
</div>
<br>
<div class="uk-container">
	<div align="center">
		<strong>Hint:</strong> If you only have one Pi, then you would not add a node.  Additional Pi's are what node's are.
	</div>
</div>



