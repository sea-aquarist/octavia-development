<style type="text/css">


#chart {
  max-width: 650px;
  margin: 35px auto;
}

</style>


<?php
include "connection.php";
$unique=mt_rand();

$myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
$thisnode = fgets($myfile);
$thisnode = str_replace('`', '', $thisnode);
fclose($myfile);
$thisnode = trim($thisnode);
$thislocalnode = $thisnode;
$thislocalnode = str_replace('masterrelay', 'Master Pi', $thislocalnode);


$stmt = $db->query("SELECT * FROM config WHERE description='log' AND node='$thisnode';");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  if ($row['set1']=='ds18b20'){$ds18b20 = $row['set4'];};
  if ($row['set1']=='dht1122'){$dht1122 = $row['set4'];};
  if ($row['set1']=='bme'){$bme = $row['set4'];};

  };


// $record_collection = array();


// $stmt = $db->query("SELECT * FROM ds18b20;");
//   while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
//     array_push($record_collection, $row['id']);    
//   };
$value=187;
print '<div style="max-width:800px;"><div id="chart'.$unique.'"></div></div>';

$table = $_GET['table'];
$attr = $_GET['attr'];
$table_id = $_GET['table_id'];
$displaytype = $_GET['displaytype'];
if ($displaytype=='ds18b20') {$display=$ds18b20;};
if ($displaytype=='dht1122') {$display=$dht1122;};
if ($displaytype=='bme') {$display=$bme;};

// $table='ds18b20';
// $attr = 'temperature';
// $table_id = 187;


$ds18b20_array = array();
$time_array = array();
$stmt = $db->query("SELECT * FROM log WHERE `table`='$table' and attr='temperature' and table_id='$table_id' ORDER BY id DESC LIMIT $display;");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    $valued = $row['value'];
    $time = $row['stamp'];
    $attrname = $row['attr'];

    $time = strtotime($time);
    $time = date("H:i:s",$time);
    $valued = $valued;
    $time = "'".$time."'";
    array_push($ds18b20_array, $valued);    
    array_push($time_array, $time);    
  };

$humidity = array();
$time_array = array();
$stmt = $db->query("SELECT * FROM log WHERE `table`='$table' and attr='humidity' and table_id='$table_id' ORDER BY id DESC LIMIT $display;");
  while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
    $valued = $row['value'];
    array_push($humidity, $valued);    
    
  };



$ds18b20_array = array_reverse($ds18b20_array);
$humidity = array_reverse($humidity);

$time_array = array_reverse($time_array);

$data = implode($ds18b20_array,",");
$data2 = implode($humidity,",");
$labels = implode($time_array,",");


print '
<script type="text/javascript">
	var options'.$unique.' = {
grid: {
	xaxis: {
		lines: {
			show: true,
			}	
		},
      },

  chart: {
    type: \'area\',
    toolbar: {
    	show: true,
    	 tools: {
          download: true,
          selection: true,
          zoom: true,
          zoomin: true,
          zoomout: true,
          pan: true,
          },

    },
  },

  series: [
  {
    name: \'Temperature:\',
    data: ['.$data.'],
  },
  {
    name: \'Humidity:\',
    data: ['.$data2.'],
  },

  ],

yaxis: {
  opposite: true,
},

  xaxis: {    
    categories: ['.$labels.']
  },

	stroke: {
  		curve: \'smooth\',
  		width:2,
  	},

	colors: [\'#77B6EA\', \'#545454\'],
	dataLabels: {
	          enabled: false,
    	    },
	}

var chart'.$unique.' = new ApexCharts(document.querySelector("#chart'.$unique.'"), options'.$unique.');

chart'.$unique.'.render();
</script>

';
?>

