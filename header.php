<?php
include "connection.php";
$stmt = $db->query("SELECT * FROM config WHERE description='logo';");
            while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $logo=$row['set1'];
                $theme=$row['set2'];
            };
?>
  <head>
        <title>Octavia</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/uikit.min.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/<?php print $theme; ?>.css" />
        <script src="js/uikit.min.js"></script>
        <script src="js/uikit-icons.min.js"></script>
        <script src="js/jquery.js"></script>
        <!-- <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@500&display=swap" rel="stylesheet"> -->
    </head>
