debug=0

import MySQLdb
import time
import dbconnection
import os

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)

checkpoint = time.time()

while True:
  try:
    f = open("mynode.txt", "r")
    nodeselected = f.read()
    nodeselected = nodeselected.replace('`','')
    nodeselected = nodeselected.replace('\n','')
    f.close()
    nodeselected = str(nodeselected)
    
    curs = db.cursor()
    curs.execute ('SELECT * FROM config where description="log" and set1="global";')
    db.commit()    
    results = curs.fetchall()
    for row in results:
      loginterval=row[3]
      loginterval = int(loginterval)
      logmax = row[4]
      logmax = int(logmax)
    # if debug==1:      
      # print ("Node: "),nodeselected,("Attempt:")
    curs = db.cursor()
    curs.execute ('SELECT * FROM ds18b20 where node="%s";' % (nodeselected))
    db.commit()
    # curs.execute ('SELECT * FROM ds18b20 where node="%s"' % ('masterrelay'))
    results = curs.fetchall()
    for row in results:
      serialnumber=row[2]            
      id_id = row[0]           
      # db.commit()
      if os.path.exists("/sys/bus/w1/devices/%s/w1_slave" % ('%s')%(serialnumber)):
        tfile = open("/sys/bus/w1/devices/%s/w1_slave" % ('%s')%(serialnumber)) 
        text = tfile.read()
        tfile.close()
        secondline = text.split("\n")[1]
        thermdata = secondline.split(" ")[9]
        level1 = thermdata.replace("t=","")
        level1 = (float(level1))
        level1 = level1/1000 #Decimel .3 for extreme resolution
        if debug==1:
          print (level1)
        therm = float(thermdata[2:])
        therm = therm / 1000
        therm = round(therm,2)
        therm = level1
        
        f = open("/mnt/octavia/%s.dat" % (serialnumber), "w" )
        f.write(str(therm))
        f.close()
        if debug==1:
          print (checkpoint)
          print (id_id)
        if loginterval!=0 and time.time() - checkpoint > loginterval :
          curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value) VALUES ('ds18b20','%s','temperature','%s','%s');" % (id_id,nodeselected,therm))
          db.commit()
      time.sleep(0.2)          

      # if debug==1:        
        # print (therm)
        # curs.execute ('INSERT INTO log SET value="%s",description="therm";' % (therm))  # TEMPORARY FOR TROUBLESHOOTING !!!
        # db.commit()                                                          # TEMPORARY FOR TROUBLESHOOTING !!!
    if time.time() - checkpoint > loginterval :
      checkpoint = time.time()
      # curs.execute ("SELECT id FROM log WHERE `table`='ds18b20' ORDER BY id DESC LIMIT %s"%(logmax))
      # results = curs.fetchall()
      # db.commit()
      # keep = []
      # for row in results: 
      #   keep.append(str(row[0]))
      # keepers = ",".join(keep)
      # # print (keepers)
      # curs.execute ("DELETE FROM log WHERE `table`='ds18b20' AND id NOT IN (%s)"%(keepers))
      # db.commit()
          
    time.sleep(1)

  except Exception as error:
    if debug==1:
      print(error)
      time.sleep(0.1)
      
    try:
      if os.path.exists("/mnt/octavia/%s.dat" % ('%s')%(serialnumber)):
        os.remove("/mnt/octavia/%s.dat" % ('%s')%(serialnumber))
        time.sleep(0.1)      
    except Exception as error:
      if debug==1:
        print(error)