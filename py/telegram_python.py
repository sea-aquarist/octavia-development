debug=0

import telegram
import time
from random import randint
import dbconnection
import MySQLdb
import datetime



dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)


now = datetime.datetime.now()
now = (now.strftime("%H:%M:%S"))
exitcount = False

while True:
    if debug==1:
        print ("Telegram Checking All Messages To Be Sent")
    try:                
        if now > "00:00:01" and now < "00:00:06":
            if exitcount == False:
                curs = db.cursor()
                curs.execute ('DELETE FROM messages WHERE DATE(timesent) NOT BETWEEN CURDATE()-1 AND CURDATE();')
                db.commit()
                exitcount = True
        if exitcount == True and now > "00:00:07":
            exitcount = False

    except Exception as e:
        if debug==1:
            print (e)         
    try:
        f = open("mynode.txt", "r")
        nodeselected = f.read()
        nodeselected = nodeselected.replace("\n","")
        nodeselected = nodeselected.replace("`","")
        f.close()

    except Exception as e:
        if debug==1:
            print (e) 
    
    try:
        curs = db.cursor()
        curs.execute ('SELECT * FROM config where description="telegramtoken" and node="%s";' % (nodeselected))
        results = curs.fetchall()
        db.commit()
        token="none"        
        for row in results:
            token = row[2]
        if token=="none":
            skip="yes"
        else:
            curs.execute ('SELECT * FROM config where description="chat_id" and node="%s";' % (nodeselected))            
            results = curs.fetchall()
            db.commit()
            for row in results:
                chat_id = row[2]
                
            bot = telegram.Bot(token='%s' %(token))
            curs.execute ('SELECT * FROM messages where node="%s";' % (nodeselected))
            results = curs.fetchall()
            db.commit()        
            for row in results:
                archive=row[5]
                theid = row[0]
                message = row[2]            
                if archive==0:
                    if debug==1:
                        print ("Trying to send messages to Telegram API")
                    bot.send_message(chat_id=chat_id, text=message,parse_mode=telegram.ParseMode.HTML)
                    # bot.send_sticker(chat_id=chat_id, sticker='CAACAgIAAxkBAAIQC182FhsFVLnYbCEAArd3BNdySsScAAIIAAPANk8Tb2wmC94am2kaBA')

                    # bot.send_photo(chat_id=chat_id, photo=open('../assets/pic.png', 'rb'))
                    # bot.send_message(chat_id=chat_id, text=message)
                    # curs.execute ('DELETE FROM messages where id="%s";' % (theid))
                    curs.execute ('UPDATE messages SET archive="1" where id="%s";' % (theid))
                db.commit()        
            message=""        
    except Exception as e:
        if debug==1:
            print (e)
    time.sleep(2)         

    
    try:
        success=0
        curs.execute ("SELECT iffy3_id,COUNT(iffy3_id) AS instances FROM messages WHERE  node='%s' AND timesent >= CURDATE() GROUP BY iffy3_id;" %(nodeselected))
        results = curs.fetchall()
        db.commit()
        for row in results:
            if row[0] == None:
                if debug==1:
                    print ("No Entries in rules")
            else:                
                for row in results:
                    rulesactid = row[0]
                    instances = row[1]
                    if debug==1:
                        print ("Rules Act id:",rulesactid,"Message Count for Today:",instances)
                    
                    curs.execute ("SELECT * FROM iffy3 where id='%s';" % (rulesactid))
                    resultsB = curs.fetchall()
                    db.commit()            
                    for rowB in resultsB:
                        rule =rowB[7]
                        alert=rowB[8]
                        alertcan=rowB[9]
                        alertcan=int(alertcan)            
                    if instances >= alertcan and alert==1:
                        if debug==1:
                            print ("TELEGRAM ALERT DISABLE")
                        # SQL DISABLE ALERT
                        curs.execute ("UPDATE iffy3 SET alert='0' WHERE id='%s' "%(rulesactid))
                        db.commit()
                        if debug==1:
                            print (rule,"ID:",rulesactid," Over Limit - set alert to 0") 
    except Exception as e:
        if debug==1:
            print (e)     
        

