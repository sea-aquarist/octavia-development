import MySQLdb
import datetime
import RPi.GPIO as GPIO
import subprocess
import dbconnection
import time
from prettytable import PrettyTable
import os
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)


# table.field_names = ["Description","State","gpio","action","y","hold","begin","now","end","esp"]
# table.add_row(["City name", "Area", "Population", "Annual Rainfall","s","s","s","s","s","s"])
# # table.add_row(["Adelaide", 1295, 1158259, 600.5])

# print (table)

table = PrettyTable()
table.field_names = ["description","ON","gpio","Act","P","hold","begin","now","end","SKIP"]

tableiffy = PrettyTable()
tableiffy.field_names = ["Rulename","2","Type & ID","Val","5","6","GPIO","Relay Name","9","node","enable","Decision"]

debug = 1

def relayon(x,y,skip):	
	if actiondb==0:		
		curs = db.cursor()
		if debug==1:
			print ("Write to SQL")
		curs.execute ('UPDATE octavia.%s SET action="1" WHERE id=%s' % (nodeselected,idd))
		db.commit()
	action = 0
	if y == 1:
		action = 1
	table.add_row ([description,bcolors.OKGREEN +"ON "+ bcolors.ENDC,gpio,action,y,hold,begin,now,end,esp])
	try:
		if action==1:
			statement = "curl --silent --output --url 'http://%s/gpio/d%son' --connect-timeout 0.5> /dev/null" % (esp,gpio)
			os.system (statement)
		else:
			statement = "curl --silent --output --url 'http://%s/gpio/d%soff' --connect-timeout 0.5> /dev/null" % (esp,gpio)
			os.system (statement)
	except Exception as e:
		if debug ==1:
			print(e)
	
def relayoff(x,y,skip):	
	if actiondb==1:
		curs = db.cursor()
		if debug==1:
			print ("Write to SQL")
		curs.execute ('UPDATE octavia.%s SET action="0" WHERE id=%s' % (nodeselected,idd))
		db.commit()
	action = 1
	if y == 1:
		action = 0
	table.add_row ([description,bcolors.FAIL + "OFF" + bcolors.ENDC,gpio,action,y,hold,begin,now,end,esp])
	try:
		if action == 0:
			statement = "curl --silent --output --url 'http://%s/gpio/d%soff' --connect-timeout 0.5> /dev/null" % (esp,gpio)			
			os.system (statement)
		else:
			statement = "curl --silent --output --url 'http://%s/gpio/d%son' --connect-timeout 0.5> /dev/null" % (esp,gpio)
			os.system (statement)
	except Exception as e:
		if debug ==1:
			print(e)
  

counter = 0
# dynamic_count = 0
while True:	
	try:
		time.sleep(1)
		if debug==1:			
			os.system('clear') # on linux / os x			
		currentday = datetime.datetime.today().weekday()
		ignore=''
		if debug==1:
			print ("\ntodays number is:",currentday)
		
		try:
			f = open("mynode.txt", "r")
			nodeselected = f.read()
			nodeselected = nodeselected.replace("\n","")
			nodeselected = nodeselected.replace("`","")
			f.close()
			if debug==1:		
				print (nodeselected)
			# print ("Node: "),(nodeselected),("Attempt:"),counter,"\n\n"


		except Exception as e:
			if debug ==1:
				print(e)

		curs = db.cursor()
		curs.execute ('SELECT * FROM octavia.%s' % (nodeselected))
		# curs.execute ('SELECT * FROM node_Workshop')
		results = curs.fetchall()
		
		for row in results:
			idd = row[0]
			alert = row[11]
			description = row[1]
			begin = row[2]
			end = row[3]
			objectname = row[4]
			csv=objectname.split(",")
			gpiocategory= csv[0]
			# duration = row[12]
			# rest = row[13]

			setup_variableA = "duration_%s" % (idd)
			globals()[setup_variableA] = row[12]

			setup_variableB = "rest_%s" % (idd)
			globals()[setup_variableB] = row[13]

			setup_variableC = "total_%s" % (idd)
			globals()[setup_variableC] = row[12]

			# setup_variableD = "dynamic_count_%s" % (idd)
			# globals()[setup_variableD] = ''



			if gpiocategory == "esp":				
				gpioid = csv[1]
				# gpioid = row[4]				
				curs.execute ('SELECT * FROM octavia.esp WHERE id=%s' % (gpioid))
				resultsB = curs.fetchall()			
				for rowB in resultsB:
					gpio=rowB[2]
					polarity=rowB[3]
					esp=rowB[4]
				print (polarity)
				intime = row[5]
				outtime = row[6]		
				hold = row[7]
				# polarity = row[8]
				actiondb = row[9]				
				# esp = row[10]
				day = row[10]
				now = datetime.datetime.now()
				now = (now.strftime("%H:%M:%S"))
				begin = (begin.strftime("%H:%M:%S"))
				end = (end.strftime("%H:%M:%S"))


			
				#exit()
				
				# if debug == 1:
				# 	print "The GPIO is = %s" % (gpio)
			
			
				# try:
				# 	GPIO.setup(int(gpio), GPIO.OUT)
				# except:
				# 	if debug==1:
				# 		print (bcolors.WARNING + "Setup the GPIO below in MAIN ROUTINE for output failed for GPIO" + bcolors.ENDC),gpio
				# 	donothing = 1
				force=0
				if hold == 0:
					hold="OFF"					
					relayoff(gpio,polarity,"NO")
					force=1
					# ignore=True
			
				if hold == 1:				
					hold="ON"
					relayon(gpio,polarity,"NO")
					force=1
					# ignore=True
			
				testday = any(i in str(currentday) for i in str(day))
			
			
				# if debug==1:
				# 	print testday,description,"day to be on is: ",day #Indicates relay and "day" it's meant to fire.
				if day=="8":
					testday=True
				if day=="":
					testday=False
					ignore=True			
				# breakpoint()
				if hold == 2 and testday==True:
					hold="AUTO"				
					if now > begin and now < end:
						if intime == 1:
							#ORIGINAL CODE
							if (globals()["duration_%s"%(idd)])==0:
								relayon(gpio,polarity,"NO")
							#ORIGINAL CODE

							# INJECTED CODE---------------------------------------------------
							
							# Database read per relay
							# duration = 5
							# rest = 5
							# Database read per relay
							# duration =  (globals()["duration_%s"%(idd)])
							# rest =  (globals()["rest_%s"%(idd)])
							# total =  (globals()["total_%s"%(idd)])
							else:
								(globals()["total_%s"%(idd)]) = (globals()["duration_%s"%(idd)]) + (globals()["rest_%s"%(idd)])	
								nowcycle = time.time()
								print ("-------------------------------------------------------")
								path = "/mnt/octavia/%scycle.start"%(idd)							
								pathdynamic = "/mnt/octavia/%sdynamic.start"%(idd)	

								test_start = os.path.exists(path)
								test_dynamic = os.path.exists(pathdynamic)	

								if test_start==False:
									f = open(pathdynamic, "w")
									dynamicanswer = f.write('1')
									f.close()
									os.chmod(pathdynamic, 0o777)
								if test_dynamic==False:
									f = open(path, "w")
									timestamp = f.write('%s'%(nowcycle))
									f.close()
									os.chmod(path, 0o777)	

								
								

								try:
									f = open(pathdynamic, "r")
									dynamicanswer = f.read()
									f.close()
								except Exception as e: print(e)	

								# if (globals()["dynamic_count_%s"%(idd)]) == 0:
								if int(dynamicanswer) == 0:
									f = open(path, "w")
									timestamp = f.write('%s'%(nowcycle))
									f.close()								
								# (globals()["dynamic_count_%s"%(idd)]) = 1
								
								try:
									f = open(pathdynamic, "w")
									dynamicanswer = f.write('1')
									f.close()
								except Exception as e: print(e)	

								f = open(path, "r")
								started = f.read()	
								f.close()
															

								print ("started: ",started)
								print ("now: ",nowcycle)	

								elapsed = nowcycle - float(started) +1	

								print ("elapsed: ",int(elapsed))	

								if int(elapsed) <= (globals()["duration_%s"%(idd)]):
									print ("POWER ON")
									relayon(gpio,polarity,"NO")	

								if int(elapsed) > (globals()["duration_%s"%(idd)]) and int(elapsed) <= (globals()["total_%s"%(idd)]):
									print ("POWER OFF")
									relayoff(gpio,polarity,"NO")	

								print ((globals()["total_%s"%(idd)]),int(elapsed))
								if int(elapsed) > (globals()["total_%s"%(idd)]):
									print ("YES")
									

								if int(elapsed) >= (globals()["total_%s"%(idd)]):
									print ("TRUE HERE")								
									(globals()["dynamic_count_%s"%(idd)])=0
									f = open(pathdynamic, "w")
									data = f.write('0')
									f.close()									

							# time.sleep(1)
							# print ("dynamic_count:",dynamicanswer)
							# print (globals())
							# INJECTED CODE---------------------------------------------------							
						else:
							relayoff(gpio,polarity,"NO")				
					else:
						if outtime == 1:
							relayon(gpio,polarity,"NO")
						else:
							relayoff(gpio,polarity,"NO")
			
				if force==1:
					donothing=1
				else:
					if str(day)=="8":
						donothing=1 #Becuase 8 means daily and it should listen to the AUTO(hold=2) rule.
					else:
						if testday!=True:					
							relayoff(gpio,polarity,bcolors.WARNING + "YES" + bcolors.ENDC)
						donothing=1
	
	
				db.commit()	
		if debug==1:
			print (table)
			table.clear_rows()
	except Exception as e:
		if debug==1:
			print(e)
	# except:
		# Fail=1