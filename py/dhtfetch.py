debug=0

import time
import os
import dbconnection
import MySQLdb

checkpoint = time.time()
dolog=0
id_id=0

dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)

while True:
    try:

        f = open("mynode.txt", "r")
        nodeselected = f.read()
        nodeselected = nodeselected.replace('`','')
        nodeselected = nodeselected.replace('\n','')
        f.close()
        nodeselected = str(nodeselected)

        curs = db.cursor()
        curs.execute ('SELECT * FROM config where description="log" and set1="global";')
        db.commit()    
        results = curs.fetchall()
        for row in results:
            loginterval=row[3]
            loginterval = int(loginterval)
            logmax = row[4]
            logmax = int(logmax)

        if loginterval!=0 and time.time() - checkpoint > loginterval :
            dolog = 1
        
        curs = db.cursor()
        curs.execute ('SELECT * FROM dht1122 where node="%s";' % (nodeselected))
        results = curs.fetchall()
        for row in results:
            gpio=row[2]
            id_id=row[0]                
            os.system('python3 /var/www/html/py/dht.py %s %s %s' % (gpio,dolog,id_id))                   
        db.commit()
        

        if time.time() - checkpoint > loginterval :
            checkpoint = time.time()
            # curs.execute ("SELECT id FROM log WHERE `table`='dht1122' ORDER BY id DESC LIMIT %s"%(logmax*2))
            # results = curs.fetchall()
            # db.commit()
            # keep = []
            # for row in results: 
            #     keep.append(str(row[0]))
            # keepers = ",".join(keep)
            # # print (keepers)
            # curs.execute ("DELETE FROM log WHERE `table`='dht1122' AND id NOT IN (%s)"%(keepers))
            # db.commit()
            dolog = 0
        time.sleep(1)
    
    except Exception as e:
        if debug==1:
            print(e)        
