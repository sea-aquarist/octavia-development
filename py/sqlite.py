
import dbconnection
import MySQLdb
import os
import json
import time
from prettytable import PrettyTable # sudo pip3 install PTable

debug=0


dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()


import sqlite3
sqliteConnection = sqlite3.connect("/mnt/octavia/test.db")
sqliteCursor = sqliteConnection.cursor()
sqliteCursor.execute("CREATE TABLE IF NOT EXISTS 'sensors' (	'id'	INTEGER,	'fromtable'	TEXT,'sensortype'	TEXT,'table_id'	INTEGER,	'description'	TEXT,	'uid'	TEXT UNIQUE,	'temperature'	REAL DEFAULT nan,	'humidity'	REAL DEFAULT nan,	'pressure'	REAL DEFAULT nan,	'altitude'	REAL DEFAULT nan,	PRIMARY KEY('id' AUTOINCREMENT));")
sqliteConnection.commit()

while True:
	try:
		try:
	 		f = open("mynode.txt", "r")
	 		nodeselected = f.read()
	 		nodeselected = nodeselected.replace("\n","")
	 		nodeselected = nodeselected.replace("`","")
	 		f.close()

		except Exception as e:
			if debug==1:
				print (e)

		temperature = "nan"
		humidity = "nan"
		pressure = "nan"
		altitude = "nan"

		sensorvalidate = []

		rows = sqliteCursor.execute("SELECT * FROM sensors").fetchall()
		sqliteConnection.commit()
		# for x in rows:
			# print (x[0],x[1],x[2],x[3],x[4],x[5])

		curs.execute ('SELECT * FROM ds18b20 where node="%s"'%(nodeselected))
		innerresults = curs.fetchall()
		db.commit()
		for innerrow in innerresults:
			fromtable = "ds18b20"
			uid = innerrow[2] #serial number
			sensorvalidate.append(uid)
			table_id = innerrow[0]
			description = innerrow[1]
			type = "ds18b20"
			sensortype = "ds18b20"
			try:
				if os.path.exists("/mnt/octavia/%s.dat" % ('%s')%(uid)):
					f = open("/mnt/octavia/%s.dat"% (uid), "r" )
					value = f.read()					
					f.close()							
					temperature = float(value)
					sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype, table_id,description,uid,temperature) VALUES ('%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature))
					sqliteConnection.commit()
					sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s' WHERE uid='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,uid))
					sqliteConnection.commit()
					


			except Exception as e:
				print(e)

		curs.execute ('SELECT * FROM dht1122 where node="%s"'%(nodeselected))
		innerresults = curs.fetchall()
		db.commit()
		for innerrow in innerresults:
			fromtable = "dht1122"
			uid = innerrow[2] #serial number
			sensorvalidate.append(str(uid))
			table_id = innerrow[0]
			description = innerrow[1]
			type = "ds18b20"
			sensortype = "dht1122"
			try:
				if os.path.exists("/mnt/octavia/%s.dat" % ('%s')%(uid)):
					f = open("/mnt/octavia/%s.dat"% (uid), "r" )
					hvalue = f.read()					
					f.close()
					f = open("/mnt/octavia/%st.dat"% (uid), "r" )
					tvalue = f.read()					
					f.close()												
					temperature = float(tvalue)
					humidity = float(hvalue)					
					sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype,table_id,description,uid,temperature,humidity) VALUES ('%s','%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature,humidity))
					sqliteConnection.commit()
					sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s',humidity='%s' WHERE uid='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,uid))
					sqliteConnection.commit()
			except Exception as e:
				print(e)


		curs.execute ('SELECT * FROM bme where node="%s"'%(nodeselected))
		innerresults = curs.fetchall()
		db.commit()
		for innerrow in innerresults:
			try:
				fromtable = "bme"
				uid = innerrow[2] #serial number
				sensorvalidate.append(uid)
				# print (uid,".json")
				table_id = innerrow[0]
				description = innerrow[1]	
				prep = ('/mnt/octavia/%s.json' %(uid))
				with open(prep, 'r') as fj:
					data = json.load(fj)
				espbme = data[8]
					
				try:		
					try:
						temperature = (espbme['temperature'])								
					except:
						temperature = "nan"
					try:
						humidity = (espbme['humidity'])
					except:
						humidity = "nan"
					try:
						pressure = (espbme['pressure'])
					except:
						pressure = "nan"
					try:
						altitude = (espbme['altitude'])
					except:
						altitude = "nan"	
					sensor = (espbme['sensor'])
					sensortype = sensor
					

					# temperature = float(0)
					# humidity = float(0)
					# pressure = float(0)
					# altitude = float(0)

					temperature = float(temperature)
					humidity = float(humidity)
					pressure = float(pressure)
					altitude = float(altitude)						
					# print (fromtable,table_id,description,uid,temperature,humidity,pressure,altitude,uid)
					# print ("\n")
				
					sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude))
					sqliteConnection.commit()			
					sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s',humidity='%s',pressure='%s',altitude='%s' WHERE uid='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude,uid))
					sqliteConnection.commit()
					
					curs.execute ('SELECT * FROM validator where node="%s" AND tablename="%s" AND table_id="%s" '%(nodeselected,fromtable,table_id))
					validatorresults = curs.fetchall()
					db.commit()
					for rows in validatorresults:
						validatorid = rows[0]
						ruletype=rows[5]
						sensorattribute=rows[4]
						if sensorattribute==0:
							testingvalue = temperature
						if sensorattribute==1:
							testingvalue = humidity
						if sensorattribute==2:
							testingvalue = pressure
						if sensorattribute==3:
							testingvalue = altitude
						A=rows[6]
						B=rows[7]
						C=rows[8]

						if ruletype == 0:
							if debug==1:
								print ("Rule 0")
							if testingvalue == A:
								if debug==1:
									print ("Case temperature = is True")
								if (C!=1):
									curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND node="%s"'%(validatorid,nodeselected))
									db.commit()
							else:
								if debug==1:
									print ("Case temperature = is False")
								if (C!=0):
									curs.execute ('UPDATE validator SET C="0" WHERE id="%s" AND node="%s"'%(validatorid,nodeselected))
									db.commit()
						if ruletype == 1:
							if debug==1:
								print ("Rule 1")
							if testingvalue < A:
								if debug==1:
									print ("Case temperature = is True")
								if (C!=1):
									curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
									db.commit()
							else:
								if debug==1:
									print ("Case temperature = is False")
								if (C!=0):
									curs.execute ('UPDATE validator SET C=0 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
									db.commit()
						if ruletype == 2:
							if debug==1:
								print ("Rule 2")
							if testingvalue > A:
								if debug==1:
									print ("Case temperature = is True")
								if (C!=1):
									curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
									db.commit()
							else:
								if debug==1:
									print ("Case temperature = is False")
								if (C!=0):
									curs.execute ('UPDATE validator SET C=0 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
									db.commit()
						if ruletype == 3:
							if debug==1:
								print ("Rule 3")
							if testingvalue > A and testingvalue < B:
								if debug==1:
									print ("Case temperature = is True")
								if (C!=1):
									curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
									db.commit()
							else:
								if debug==1:
									print ("Case temperature = is False")
								if (C!=0):
									curs.execute ('UPDATE validator SET C=0 WHERE id="%s" AND node="%s"'%(validatorid,nodeselected))
									db.commit()

				
				except Exception as e:			
					if debug==1:
						print(e)
			except Exception as e:				
				sqliteCursor.execute("UPDATE sensors SET temperature='0',humidity='0',pressure='0',altitude='0' WHERE table_id='%s';" % (table_id))
				sqliteConnection.commit()
				if debug==1:
					print(e)



		if debug==1:			
			table = PrettyTable()
			table.field_names = ["1","2","3","4","5","6","7","8","9","10"]
			rows = sqliteCursor.execute("SELECT * FROM sensors").fetchall()
			sqliteConnection.commit()
			for x in rows:
				table.add_row([x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9]])	
				if x[5] not in sensorvalidate:
					print ("Not IN:",x[5])
					sqliteCursor.execute("DELETE FROM sensors WHERE uid='%s'"%(x[5]))
					sqliteConnection.commit()
				# print (x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8])
			print (table)
			print ("\n")
	except Exception as e:
		if debug==1:
			print(e)
	time.sleep(2)