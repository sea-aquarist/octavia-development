debug=1
import time
import os
import dbconnection
import MySQLdb
import json


dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()

checkpoint = time.time()
while True:
	try:
		time.sleep(1)
		# print ("START")		
		f = open("mynode.txt", "r")
		nodeselected = f.read()
		nodeselected = nodeselected.replace("\n","")
		nodeselected = nodeselected.replace("`","")
		f.close()

		curs = db.cursor()
		curs.execute ('SELECT * FROM config where description="log" and set1="global";')
		db.commit()
		results = curs.fetchall()
		for row in results:
			loginterval=row[3]
			loginterval = int(loginterval)
			# logmax = row[4]
			# logmax = int(logmax)

		# if debug==1:		
			# print (nodeselected)		
		# if debug==1:
			# print ("START")		
		curs.execute ('SELECT ipaddr FROM esp WHERE node="%s"'%(nodeselected))
		results = curs.fetchall()
		results = list(dict.fromkeys(results))
		db.commit()		
		for row in results:
			dnsname = row[0]			
			# if debug == 1:
			# 	print ('curl %s >%s.json 2>/dev/null' %(dnsname,dnsname))
			try:
				x = os.system('curl %s >/mnt/octavia/%s.json.temp  --connect-timeout 0.5 2>/dev/null' %(dnsname,dnsname))
				x = os.system('cp /mnt/octavia/%s.json.temp /mnt/octavia/%s.json 2>/dev/null' %(dnsname,dnsname))			
			except Exception as e:
				if debug==1:				
					print(e)
		
		# time.sleep(1)
		curs.execute ('SELECT * FROM bme WHERE node="%s"'%(nodeselected))
		results = curs.fetchall()
		results = list(dict.fromkeys(results))		
		db.commit()
		# print(results,"----<")
		for row in results:
			dnsname = row[2]
			# print (dnsname,"pulled from table bme")
			id_id = row[0]			
			# if debug == 1:
				# print ('curl %s >%s.json 2>/dev/null LEVEL 2' %(dnsname,dnsname))
			try:
				x = os.system('curl %s >/mnt/octavia/%s.json.temp  --connect-timeout 0.5 2>/dev/null' %(dnsname,dnsname))
				x = os.system('cp /mnt/octavia/%s.json.temp /mnt/octavia/%s.json 2>/dev/null' %(dnsname,dnsname))				
				# print (loginterval,"▄▄")
				# print (dnsname, "\t",time.time(),"\t",checkpoint,"\t", loginterval,"\t")
				# if loginterval!=0 and time.time() - checkpoint > loginterval :
				# 	# print ("-----------",dnsname)
				# 	# if debug==1:
				# 		# print ("+++++++++++++++++++++++++++++++++++++++++%s+++%s++++++++++++++++++++++++++++++++++++++++++++++++++++"%(dnsname,loginterval))
				# 	prep = ('/mnt/octavia/%s.json' %(dnsname))
				# 	with open(prep, 'r') as fj:
				# 		data = json.load(fj)
				# 	espbme = data[8]
				# 	# if debug==1:
				# 		# print ("------")
				# 		# print (espbme)
				# 		# print ("------")
					
				# 	try:
				# 		temperature = (espbme['temperature'])					
				# 	except:
				# 		skip=1
				# 		# print ("No Temperature")
				# 	try:
				# 		humidity = (espbme['humidity'])					
				# 	except:
				# 		skip=1
				# 		# print ("No Humidity")
				# 	try:
				# 		pressure = (espbme['pressure'])				
				# 	except:
				# 		skip=1
				# 		# print ("No Pressure")
				# 	try:
				# 		altitude = (espbme['altitude'])
				# 	except:
				# 		skip=1
				# 		# print ("No altitude")					
				# 	try:
				# 		sensor = (espbme['sensor'])
				# 		# print ("Sensor type : ",sensor)
				# 	except:
				# 		if debug==1:
				# 			print ("No Sensor Field ? - Possible old .ino version")				

				# 	# if debug==1:
				# 		# print ("----------------------------------------------------%s"%(sensor))
					
				# 	if sensor == "ds18b20":
				# 		curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','temperature','%s','%s','ds18b20');" % (id_id,nodeselected,float(temperature)))
				# 		db.commit()						

				# 	if sensor == "dht":
				# 		curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','temperature','%s','%s','dht');" % (id_id,nodeselected,float(temperature)))
				# 		db.commit()
				# 		curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','humidity','%s','%s','dht');" % (id_id,nodeselected,float(humidity)))
				# 		db.commit()

				# 	if sensor == "bme":
				# 		# if debug==1:
				# 			# print ("Processing BME")
				# 		# print ("catch2")
				# 		# print(temperature)					
				# 		curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','temperature','%s','%s','bme');" % (id_id,nodeselected,float(temperature)))
				# 		db.commit()
				# 		curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','pressure','%s','%s','bme');" % (id_id,nodeselected,float(pressure)))
				# 		db.commit()
				# 		curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','humidity','%s','%s','bme');" % (id_id,nodeselected,float(humidity)))
				# 		db.commit()
				# 		curs.execute ("INSERT INTO `log` (`table`, table_id,attr, node,value,sensor) VALUES ('bme','%s','altitude','%s','%s','bme');" % (id_id,nodeselected,float(altitude)))
				# 		db.commit()

					
			except Exception as e:
				if debug==1:				
					print(e)
		# print ("------------------------------------------------------------------")
		if time.time() - checkpoint > loginterval :
			checkpoint = time.time()
	except Exception as e:
		if debug==1:				
			print(e)