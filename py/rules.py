debug=0
import time
import dbconnection
import sys
import os
import MySQLdb
import subprocess
from datetime import datetime
from multiprocessing import Process
import RPi.GPIO as GPIO
import json
from prettytable import PrettyTable # sudo pip3 install PTable


GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)


dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()


import sqlite3
sqliteConnection = sqlite3.connect("/mnt/octavia/test.db")
sqliteCursor = sqliteConnection.cursor()
sqliteCursor.execute("CREATE TABLE IF NOT EXISTS 'sensors' (    'id'    INTEGER,    'fromtable' TEXT,'sensortype'   TEXT,'table_id' INTEGER,    'description'   TEXT,   'uid'   TEXT UNIQUE,    'temperature'   REAL DEFAULT nan,   'humidity'  REAL DEFAULT nan,   'pressure'  REAL DEFAULT nan,   'altitude'  REAL DEFAULT nan,   PRIMARY KEY('id' AUTOINCREMENT));")
sqliteConnection.commit()


runloop=1
while runloop ==1:      
    try:
        f = open("mynode.txt", "r")        
        nodeselected = f.read()
        nodeselected = nodeselected.replace("\n","")
        nodeselected = nodeselected.replace("`","")
        f.close()
        time.sleep(1)
        curs.execute ("SELECT * FROM iffy2 WHERE node='%s' ORDER BY id" % (nodeselected))
        results = curs.fetchall()
        db.commit()
        for row in results:
            id = row[0]        
            description = row[1]
            ruletype = row[2]
            a = row[3]
            b = row[4]
            c = row[5]
            c = int(c)
            testResult = c
            objectname = row[6]            
            try:        
                csv=objectname.split(",")
                sensor = csv[0]
                sensorid = csv[1]
                if debug==1:
                    print (sensor)
                    print (sensorid)

                if sensor == "time":
                    test=0

                if sensor == "gpio":
                    curs.execute ('SELECT * FROM gpio where id=%s' % (sensorid))
                    results = curs.fetchall()
                    for row in results:
                        gpioToRead = row[2]
                    GPIO.setup(int(gpioToRead), GPIO.OUT)
                    if (GPIO.input(int(gpioToRead))==0):                    
                        test = 0
                    else:                    
                        test = 1
                
                if sensor == "esp":
                    if debug==1:
                        print ("ESP")
                    curs.execute ('SELECT * FROM esp where id=%s' % (sensorid))
                    results = curs.fetchall()
                    for row in results:
                        espgpio = row[2]
                        ipaddr=row[4]
                        # print (ipaddr)
                        # print (espgpio)
                        statement = ('/mnt/octavia/%s.json' %(ipaddr))
                    with open(statement, 'r') as f:
                    # with open('/mnt/octavia/192.168.0.23.json', 'r') as f:
                        data = json.load(f)
                        f.close()
                    for x in data:
                        # print ("DATA STAGE")
                        # print(x['gpio'])
                        espgpiojson=(x['gpio'])
                        espgpiojsonStatus=(x['status'])
                        # print (espgpiojson)
                        if int(espgpiojson) == int(espgpio):
                            # print ("found",x['gpio'])
                            # print ("status",x['status'])
                            # print ("match")                    
                            if (int(espgpiojsonStatus)==0):                    
                                test = 0
                                # print ("status is 0")
                            else:                    
                                test = 1
                                # print ("status is 1")


                if sensor == "ds18b20":			
                    curs.execute ('SELECT * FROM ds18b20 where id="%s"' % (sensorid))
                    innerresults = curs.fetchall()
                    for innerrow in innerresults:
                        serialnumber = innerrow[2]				
                        try:
                            if os.path.exists("/mnt/octavia/%s.dat" % ('%s')%(serialnumber)):
                                f = open("/mnt/octavia/%s.dat"% (serialnumber), "r" )
                                value = f.read()					
                                f.close()                            
                                value_one = float(value)
                            # print (value_one)					
                        except Exception as e:
                            if debug==1:
                                print(e)
                    test = value_one

                if sensor == "dht1122":			
                    curs.execute ('SELECT * FROM dht1122 where id="%s"' % (sensorid))
                    innerresults = curs.fetchall()
                    for innerrow in innerresults:
                        serialnumber = innerrow[2]				
                        try:
                            if os.path.exists("/mnt/octavia/%s.dat" % ('%s')%(serialnumber)):
                                f = open("/mnt/octavia/%s.dat"% (serialnumber), "r" )
                                value = f.read()					
                                f.close()
                                value_one = float(value)
                            # print (value_one)					
                        except Exception as e:
                            if debug==1:
                                print(e)
                    test = value_one
            except Exception as e:
                if debug==1:
                    print(e)
                    skip = 1
            
            test = (float(test))
            
            try:
                # ---------------------------------------------------------------------------------------------
                if ruletype == 0:                   #Between numbers
                    a = float(a)        
                    b = float(b)
                    if a < test < b:                
                        testResult = 1
                        if debug == 1:
                            print ("Rule 0 : %s : %s : %s" % (testResult,description,test))
                    else:
                        testResult = 0
                        if debug == 1:
                            print ("Rule 0 : %s : %s : %s" % (testResult,description,test))
                # ---------------------------------------------------------------------------------------------
                if ruletype == 1:
                    if debug==1:
                        print ("HERE")                   #Time - Between two time's before 12AM or After 12AM            
                        print ("a:",a)                    
                    a = datetime.strptime(a, '%H:%M:%S')
                    b = datetime.strptime(b, '%H:%M:%S')
                    if debug==1:
                        print (a,b)                    
                    dt = datetime.now()
                    now = (dt.strftime("%H:%M:%S"))            
                    nowToString = str(now)
                    currentTime = datetime.strptime(nowToString, '%H:%M:%S')
                    test = currentTime
                    if a < test < b:
                        testResult = 1
                        if debug == 1:
                            print ("Rule 1 : %s : %s : %s" % (testResult,description,test))
                        
                    else:
                        testResult = 0
                        if debug == 1:
                            print ("Rule 1 : %s : %s : %s" % (testResult,description,test))                    
                    
                # ---------------------------------------------------------------------------------------------
                if ruletype == 2:                   #Absolute - number is equal too
                    a = float(a)
                    if test == a:
                        testResult = 1
                        if debug == 1:
                            print ("Rule 2 : %s : %s : %s" % (testResult,description,test))
                    else:
                        testResult = 0
                        if debug == 1:
                            print ("Rule 2 : %s : %s : %s" % (testResult,description,test))
                # ---------------------------------------------------------------------------------------------
                if ruletype == 3:                   #Gpio - gpio state of true or false / on/off
                    a = float(a)
                    if test == a :
                        testResult = 1
                        if debug == 1:
                            print ("Rule 3 : %s : %s : %s" % (testResult,description,test))
                    else:
                        testResult = 0
                        if debug == 1:
                            print ("Rule 3 : %s : %s : %s" % (testResult,description,test))      
                # ---------------------------------------------------------------------------------------------
                
                if c == testResult:
                    skipwrite = 1
                else:
                    if debug == 1:
                        print ("Write to SQL")
                    curs.execute ("UPDATE iffy2 SET C='%s' WHERE id='%s' and node='%s'" % (testResult,id,nodeselected))
                    db.commit()
            # except Exception as e: print(e)
            except Exception as e:
                if debug==1:
                    print(e)       # print (testResult)     
        db.commit()
    except Exception as e:
        if debug==1:
            print(e)       # print (testResult)     
    try:
        try:
            f = open("mynode.txt", "r")
            nodeselected = f.read()
            nodeselected = nodeselected.replace("\n","")
            nodeselected = nodeselected.replace("`","")
            f.close()

        except Exception as e:
            if debug==1:
                print (e)

        temperature = "nan"
        humidity = "nan"
        pressure = "nan"
        altitude = "nan"

        sensorvalidate = []

        rows = sqliteCursor.execute("SELECT * FROM sensors").fetchall()
        sqliteConnection.commit()
        # for x in rows:
            # print (x[0],x[1],x[2],x[3],x[4],x[5])

        curs.execute ('SELECT * FROM ds18b20 where node="%s"'%(nodeselected))
        innerresults = curs.fetchall()
        db.commit()
        for innerrow in innerresults:
            fromtable = "ds18b20"
            uid = innerrow[2] #serial number
            sensorvalidate.append(uid)
            table_id = innerrow[0]
            description = innerrow[1]
            type = "ds18b20"
            sensortype = "ds18b20"
            try:
                if os.path.exists("/mnt/octavia/%s.dat" % ('%s')%(uid)):
                    f = open("/mnt/octavia/%s.dat"% (uid), "r" )
                    value = f.read()                    
                    f.close()                           
                    temperature = float(value)
                    sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype, table_id,description,uid,temperature) VALUES ('%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature))
                    sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s' WHERE uid='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,uid))
                    sqliteConnection.commit()
                    


            except Exception as e:
                print(e)

        curs.execute ('SELECT * FROM dht1122 where node="%s"'%(nodeselected))
        innerresults = curs.fetchall()
        db.commit()
        for innerrow in innerresults:
            fromtable = "dht1122"
            uid = innerrow[2] #serial number
            sensorvalidate.append(str(uid))
            table_id = innerrow[0]
            description = innerrow[1]
            type = "ds18b20"
            sensortype = "dht1122"
            try:
                if os.path.exists("/mnt/octavia/%s.dat" % ('%s')%(uid)):
                    f = open("/mnt/octavia/%s.dat"% (uid), "r" )
                    hvalue = f.read()                   
                    f.close()
                    f = open("/mnt/octavia/%st.dat"% (uid), "r" )
                    tvalue = f.read()                   
                    f.close()                           
                    temperature = float(tvalue)
                    humidity = float(hvalue)
                    sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype,table_id,description,uid,temperature,humidity) VALUES ('%s','%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature,humidity))
                    sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s',humidity='%s' WHERE uid='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,uid))
                    sqliteConnection.commit()
            except Exception as e:
                print(e)


        curs.execute ('SELECT * FROM bme where node="%s"'%(nodeselected))
        innerresults = curs.fetchall()
        db.commit()
        for innerrow in innerresults:
            try:
                fromtable = "bme"
                uid = innerrow[2] #serial number
                sensorvalidate.append(uid)
                # print (uid,".json")
                table_id = innerrow[0]
                description = innerrow[1]   
                prep = ('/mnt/octavia/%s.json' %(uid))
                with open(prep, 'r') as fj:
                    data = json.load(fj)
                espbme = data[8]
                # print (espbme)    
                try:        
                    try:
                        temperature = (espbme['temperature'])                               
                    except:
                        temperature = "nan"
                    try:
                        humidity = (espbme['humidity'])
                    except:
                        humidity = "nan"
                    try:
                        pressure = (espbme['pressure'])
                    except:
                        pressure = "nan"
                    try:
                        altitude = (espbme['altitude'])
                    except:
                        altitude = "nan"    
                    sensor = (espbme['sensor'])
                    sensortype = sensor
                    
                    temperature = float(temperature)
                    humidity = float(humidity)
                    pressure = float(pressure)
                    altitude = float(altitude)      
                
                    # print (fromtable,table_id,description,uid,temperature,humidity,pressure,altitude,uid)
                    # print ("\n")
                
                    sqliteCursor.execute("INSERT OR IGNORE INTO sensors (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude) VALUES ('%s','%s','%s','%s','%s','%s','%s','%s','%s');" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude))
                    sqliteConnection.commit()           
                    sqliteCursor.execute("UPDATE sensors SET fromtable='%s',sensortype='%s',table_id='%s',description='%s',uid='%s',temperature='%s',humidity='%s',pressure='%s',altitude='%s' WHERE uid='%s';" % (fromtable,sensortype,table_id,description,uid,temperature,humidity,pressure,altitude,uid))
                    sqliteConnection.commit()
                    curs.execute ('SELECT * FROM validator where node="%s" AND tablename="%s" AND table_id="%s" '%(nodeselected,fromtable,table_id))
                    validatorresults = curs.fetchall()
                    db.commit()
                    for rows in validatorresults:
                        validatorid = rows[0]
                        ruletype=rows[5]
                        sensorattribute=rows[4]
                        if sensorattribute==0:
                            testingvalue = temperature
                        if sensorattribute==1:
                            testingvalue = humidity
                        if sensorattribute==2:
                            testingvalue = pressure
                        if sensorattribute==3:
                            testingvalue = altitude
                        A=rows[6]
                        B=rows[7]
                        C=rows[8]

                        if ruletype == 0:
                            if debug==1:
                                print ("Rule 0")
                            if testingvalue == A:
                                if debug==1:
                                    print ("Case temperature = is True")
                                if (C!=1):
                                    curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND node="%s"'%(validatorid,nodeselected))
                                    db.commit()
                            else:
                                if debug==1:
                                    print ("Case temperature = is False")
                                if (C!=0):
                                    curs.execute ('UPDATE validator SET C="0" WHERE id="%s" AND node="%s"'%(validatorid,nodeselected))
                                    db.commit()
                        if ruletype == 1:
                            if debug==1:
                                print ("Rule 1")
                            if testingvalue < A:
                                if debug==1:
                                    print ("Case temperature = is True")
                                if (C!=1):
                                    curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
                                    db.commit()
                            else:
                                if debug==1:
                                    print ("Case temperature = is False")
                                if (C!=0):
                                    curs.execute ('UPDATE validator SET C=0 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
                                    db.commit()
                        if ruletype == 2:
                            if debug==1:
                                print ("Rule 2")
                            if testingvalue > A:
                                if debug==1:
                                    print ("Case temperature = is True")
                                if (C!=1):
                                    curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
                                    db.commit()
                            else:
                                if debug==1:
                                    print ("Case temperature = is False")
                                if (C!=0):
                                    curs.execute ('UPDATE validator SET C=0 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
                                    db.commit()
                        if ruletype == 3:
                            if debug==1:
                                print ("Rule 3")
                            if testingvalue > A and testingvalue < B:
                                if debug==1:
                                    print ("Case temperature = is True")
                                if (C!=1):
                                    curs.execute ('UPDATE validator SET C=1 WHERE id="%s" AND  node="%s"'%(validatorid,nodeselected))
                                    db.commit()
                            else:
                                if debug==1:
                                    print ("Case temperature = is False")
                                if (C!=0):
                                    curs.execute ('UPDATE validator SET C=0 WHERE id="%s" AND node="%s"'%(validatorid,nodeselected))
                                    db.commit()

                
                except Exception as e:          
                    print(e)
            except Exception as e:
                print(e)



        if debug==1:            
            table = PrettyTable()
            table.field_names = ["1","2","3","4","5","6","7","8","9","10"]
            rows = sqliteCursor.execute("SELECT * FROM sensors").fetchall()
            sqliteConnection.commit()
            for x in rows:
                table.add_row([x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8],x[9]])  
                if x[5] not in sensorvalidate:
                    print ("Not IN:",x[5])
                    sqliteCursor.execute("DELETE FROM sensors WHERE uid='%s'"%(x[5]))
                    sqliteConnection.commit()
                # print (x[0],x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8])
            print (table)
            print ("\n")
    except Exception as e:
        print(e)
    