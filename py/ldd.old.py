debug=0

from Adafruit_PWM_Servo_Driver import PWM
import time
import dbconnection
import sys
import MySQLdb
import subprocess
from multiprocessing import Process


dbhost = dbconnection.dbhost()
dbuser = dbconnection.dbuser()
dbpasswd = dbconnection.dbpasswd()
dbname = dbconnection.dbname()
db = MySQLdb.connect (host = dbhost, user= dbuser, passwd=dbpasswd,db = dbname)
curs = db.cursor()

# Initialise the PWM device using the default address

pwm = PWM(0x40)
pwm.setPWMFreq(60)                        # Set frequency to 60 Hz

def leddim(channel,a,b,c):
	if debug == 1:
		print ("-----------------")
		print ("channel:",channel)
		print ("Seeing 2 if set to off is norrmal- CURRENT STEP:",c)
	for x in range (a,b,c):
		if debug ==1:
			print (x)
		c = abs(c)
		# if b==1: #b is the end :) not the start
			# pwm.setPWM(channel,0,4097)
			# break			
		# else:
		pwm.setPWM(channel,0,x)
		if c ==1:
			c=0.50
		if c ==2:
			c=0.09
		if c ==3:
			c=0.01
		if c ==4:
			c=0.001
		if c ==5:
			c = 0.0
		time.sleep(c)
	if b==1: #b is the end :) not the start
		pwm.setPWM(channel,0,4096)		

def setled (channel,pwmrate,pwmratelast):
	# print ("HELLO")
	# print (pwmrate)	
	if pwmratelast>pwmrate:
		for do in range(pwmratelast,pwmrate,-8):				
			pwm.setPWM(channel,0,do)			
	for do in range(pwmratelast,pwmrate,+8):
		pwm.setPWM(channel,0,do)
	if pwmrate==0:		
		pwm.setPWM(channel,0,1) #0 didnt seem to switch the LDD off ? this might differ with other LED's circle back if it doesnt.

	curs.execute ('UPDATE pca9685 SET run=0 where channel=%s'%(pwmchannel))
	curs.execute ('UPDATE pca9685 SET inprogress=0 WHERE channel=%s' % (pwmchannel))
	db.commit()
	


def rampup(y):
	for x in range (0,4096,y):
		pwm.setPWM(0,0, x)
		#time.sleep(0.01)

def rampdown(z):
	for x in range (4096,0,z):
		pwm.setPWM(0,0,x)

# pwm.setPWM(15,0,0)
x = 1
while x == 1:
	try:
		time.sleep(0.5)
		curs.execute ('SELECT * FROM pca9685')
		results = curs.fetchall()
		for row in results:
			pwmchannel = (row[2])				
			pwmrate = (row[3])
			pwmratelast = (row[4])
			run=(row[7])
			inprogress=(row[8])
			# if debug == 1:
				# print (pwmrate)
			if inprogress==1:
				skip=("yes")
			else:
				if run==1:
					curs.execute ('UPDATE pca9685 SET inprogress=1 WHERE channel=%s' % (pwmchannel))
					db.commit()
					setled(pwmchannel,pwmrate,pwmratelast)
				# Process(target=setled, args=(pwmchannel,pwmrate,pwmratelast)).start()
		db.commit()
	except Exception as e:
		if debug==1:
			print(e)
	# Process(target=setled, args=(pwmchannel,pwmrate,pwmratelast)).start()
	# Process(target=leddim, args=(channel,start,end,speed)).start()
	# leddim(channel,(manual-1),manual,1)