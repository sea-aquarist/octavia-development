<?php
include "connection.php";
include "header.php";
include "nav.php";

$myfile = fopen("py/mynode.txt", "r") or die("Unable to open file!");
		$thislocalnode = fgets($myfile);
		$thislocalnode = str_replace('`', '', $thislocalnode);
		fclose($myfile);
		$thislocalnode = trim($thislocalnode);
        $value=$thislocalnode;
        
?>

<div align="center">
<style>
    .uk-select {
    font-size:15px;    
    };
</style>


<div class="uk-card uk-card-default uk-card-body uk-width-1-3@m" style="padding:20px !important;">
    <h3 class="uk-card-title">THIS NODE IS</h3>

 <div align="left" style="text-align: justify;">
<p>
The following configuration setting, causes all scripts for hardware interactions to execute locally on this Raspberry Pi. This setting is different to the view buttons that appear below the navigation bar (top right) shown only hen accessing pages other than this one.
</p>
<p>
The view buttons allow you to flip between Raspberry Pi the Master and the Nodes, and alter their configurations.  This will not interfere with what is connected to this this Raspberry Pi.
</p>
<p>
In order to have multiple nodes or more than one Pi, each has to have it's own identity. Having an identity allows you to switch identities also, say for example one Raspberry pi died, you could move another node into it's place, connect all the sensors and just set it to the previous node that died, all configurations will automatically attach to sensors. The first Raspbery Pi in your node collection is MASTER PI, this cannot be changed, you have the ability to name "NEW" nodes.
</p>
</div>

<form action="submit.php" method="POST">
	<input name="option" value="setnodeto" hidden>

<?php
$addpitables = array();
$stmt = $db->query("SHOW TABLES WHERE Tables_in_octavia LIKE 'node_%';");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		$tablename = $row['Tables_in_octavia'];
		array_push($addpitables, $tablename);
	;};
array_push($addpitables, 'masterrelay');



print '<select class="uk-select " style="width:250px;" name="setnodeto">';
foreach ($addpitables as $key => $value) {
	if ($value==$thislocalnode) {$selected="selected";} else {$selected="";};
	$valuelabel=str_replace("node_", "", $value);
	$valuelabel=str_replace("_", " ", $valuelabel);
    if ($valuelabel=="masterrelay") {$valuelabel="MASTER PI";};
    print '
		<option value="'.$value.'"  '.$selected.'>'.strtoupper($valuelabel).'
	';
	# code...
	// print $value.$thisnode;
};
print '</select>';
?>
<button class="<?php print $theme;?> uk-button uk-button-default save-button" style="" type="submit">Set</button>&nbsp
</form>
	<!-- THIS NODE IS : -->


<p>To make it easier to identify which physical node this reperesents, you can set the background color for this node here.</p>
<form action="submit.php" method="POST">
	<input name="option" value="setcolor" hidden>
	<input name="localnode" value="<?php print $thislocalnode;?>" hidden>
<?php
$stmt = $db->query("SELECT * FROM config where description='thisnode' and node='$thislocalnode';");
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)) { 
		$set2=$row['set2'];
	};


print ' 
 <link rel="stylesheet" href="colorpick/la_color_picker.css">
  
  <input class="uk-input" name="colorpick" value='.$set2.' id="colorPicker" style="width:100px;">
  <div class="palette" id="colorPalette"></div>
  <script src="colorpick/la_color_picker.js"></script>

';
?>
<button class="<?php print $theme;?> uk-button uk-button-default save-button" style="" type="submit">Set</button>&nbsp
</form>    
	<!-- </div>
</div> -->
</p>
<img src="assets/raspberrypi.png" width="100">
</div>    
</div>