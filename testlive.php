<?php
include "connection.php";
include "header.php";
include "nav.php";

?>
<meta http-equiv="refresh" content="300">

<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script> -->


<div class="livedata1"></div>
<div class="livedata2"></div>


<!-- ****** PULL STATE CONSTANTLY ****** -->
<script type="text/javascript">
  RefreshData();
  setInterval( RefreshData, 5000 );
  var inRequestb = false;
  function RefreshData() {
    if ( inRequestb ) {
      return false;
    }
    inRequestb = true;
    var load = $.get('rulesiffy3.php');
    $(".livedata1");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".livedata1").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestb = false;
    });
  }
</script>

<script type="text/javascript">
  RefreshData();
  setInterval( RefreshData, 5000 );
  var inRequestb = false;
  function RefreshData() {
    if ( inRequestb ) {
      return false;
    }
    inRequestb = true;
    var load = $.get('rulesvalidator.php');
    $(".livedata2");
    load.error(function() {
      console.log( "Error" );
      // do something here if request failed
    });
    load.success(function( res ) {
      console.log( "Success" );
      $(".livedata2").html(res);
    });
    load.done(function() {
      console.log( "Completed" );
      inRequestb = false;
    });
  }
</script>

